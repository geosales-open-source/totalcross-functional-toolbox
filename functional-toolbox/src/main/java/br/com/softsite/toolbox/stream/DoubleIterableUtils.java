package br.com.softsite.toolbox.stream;

import java.util.Iterator;
import java.util.NoSuchElementException;

class DoubleIterableUtils {

	private DoubleIterableUtils() {
	}

	static DoubleIterable toDoubleIterable(Iterable<Double> iterable) {
		return () -> new DoubleIterator() {
			Iterator<Double> iterator = iterable.iterator();
			
			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}
			
			@Override
			public double next() {
				return iterator.next();
			}
		};
	}

	static Iterable<Double> toIterable(DoubleIterable iterable) {
		return () -> new Iterator<Double>() {
			DoubleIterator iterator = iterable.iterator();
			
			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}
			
			@Override
			public Double next() {
				return iterator.next();
			}
		};
	}

	static DoubleIterable toDoubleIterable(double[] a) {
		return () -> new DoubleIterator() {
			int idx = 0;
			
			@Override
			public boolean hasNext() {
				return idx < a.length;
			}
			
			@Override
			public double next() {
				if (idx >= a.length) {
					throw new NoSuchElementException("End of iteration");
				}
				double e = a[idx];
				idx++;
				return e;
			}
		};
	}

	static DoubleIterable toDoubleIterable(double a) {
		return () -> new DoubleIterator() {
			boolean hasNext = true;
			
			@Override
			public boolean hasNext() {
				return hasNext;
			}
			
			@Override
			public double next() {
				if (!hasNext) {
					throw new NoSuchElementException("End of iteration");
				}
				hasNext = false;
				return a;
			}
		};
	}

}
