package br.com.softsite.toolbox.collection;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class ImmutableList<T> implements List<T> {

	private final List<T> underlying;

	ImmutableList(List<T> underlying) {
		this.underlying = underlying;
	}

	@Override
	public int size() {
		return underlying.size();
	}

	@Override
	public boolean isEmpty() {
		return underlying.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return underlying.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		return new ImmutableIterator<>(underlying.iterator());
	}

	@Override
	public Object[] toArray() {
		return underlying.toArray();
	}

	@Override
	public <E> E[] toArray(E[] a) {
		return underlying.toArray(a);
	}

	@Override
	public boolean add(T e) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return underlying.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public T get(int index) {
		return underlying.get(index);
	}

	@Override
	public T set(int index, T element) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public void add(int index, T element) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public T remove(int index) {
		throw new UnsupportedOperationException("ImmutableList cannot alter its elements");
	}

	@Override
	public int indexOf(Object o) {
		return underlying.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return underlying.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return new ImmutableListIterator<>(underlying.listIterator());
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return new ImmutableListIterator<>(underlying.listIterator(index));
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return new ImmutableList<>(underlying.subList(fromIndex, toIndex));
	}
}
