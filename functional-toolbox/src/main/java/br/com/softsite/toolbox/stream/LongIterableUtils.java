package br.com.softsite.toolbox.stream;

import java.util.Iterator;
import java.util.NoSuchElementException;

class LongIterableUtils {

	private LongIterableUtils() {
	}

	static LongIterable toLongIterable(Iterable<Long> iterable) {
		return () -> new LongIterator() {
			Iterator<Long> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public long next() {
				return iterator.next();
			}
		};
	}

	static Iterable<Long> toIterable(LongIterable iterable) {
		return () -> new Iterator<Long>() {
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public Long next() {
				return iterator.next();
			}
		};
	}

	static LongIterable toLongIterable(long[] a) {
		return () -> new LongIterator() {
			int idx = 0;

			@Override
			public boolean hasNext() {
				return idx < a.length;
			}

			@Override
			public long next() {
				if (idx >= a.length) {
					throw new NoSuchElementException("End of iteration");
				}
				long e = a[idx];
				idx++;
				return e;
			}
		};
	}

	static LongIterable toLongIterable(long a) {
		return () -> new LongIterator() {
			boolean hasNext = true;

			@Override
			public boolean hasNext() {
				return hasNext;
			}

			@Override
			public long next() {
				if (!hasNext) {
					throw new NoSuchElementException("End of iteration");
				}
				hasNext = false;
				return a;
			}
		};
	}

}
