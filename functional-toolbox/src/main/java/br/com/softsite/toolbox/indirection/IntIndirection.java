package br.com.softsite.toolbox.indirection;

public class IntIndirection {
	public int x;
	
	public IntIndirection() {
	}
	
	public IntIndirection(int x) {
		this.x = x;
	}

	public int getX() {
		return x;
	}
}
