package br.com.softsite.toolbox;

public interface HasLabel {
	String getLabel();
}
