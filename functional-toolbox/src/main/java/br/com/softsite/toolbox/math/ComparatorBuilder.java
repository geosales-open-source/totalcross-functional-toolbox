package br.com.softsite.toolbox.math;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Function;

public class ComparatorBuilder<T> {
	private ArrayList<Comparator<T>> comparatorList = new ArrayList<>();
	public static <V, X extends Comparable<X>> Comparator<V> generateComparatorAccessor(Function<V, X> comparing) {
		return (a1, a2) -> comparing.apply(a1).compareTo(comparing.apply(a2));
	}

	public static <V, X> Comparator<V> generateComparatorAccessor(Function<V, X> comparing, Comparator<X> comparator) {
		return (a1, a2) -> comparator.compare(comparing.apply(a1), comparing.apply(a2));
	}

	public static <V, X extends Comparable<X>> ComparatorBuilder<V> start(Function<V, X> comparing) {
		ComparatorBuilder<V> comparatorBuilder = new ComparatorBuilder<>();
		comparatorBuilder.comparatorList.add(generateComparatorAccessor(comparing));
		return comparatorBuilder;
	}

	public static <V, X> ComparatorBuilder<V> start(Function<V, X> comparing, Comparator<X> comparator) {
		ComparatorBuilder<V> comparatorBuilder = new ComparatorBuilder<>();
		comparatorBuilder.comparatorList.add(generateComparatorAccessor(comparing, comparator));
		return comparatorBuilder;
	}

	public <X extends Comparable<X>> ComparatorBuilder<T> adicionaDesempate(Function<T, X> comparing) {
		comparatorList.add(generateComparatorAccessor(comparing));
		return this;
	}

	public <X> ComparatorBuilder<T> adicionaDesempate(Function<T, X> comparing, Comparator<X> comparator) {
		comparatorList.add(generateComparatorAccessor(comparing, comparator));
		return this;
	}

	public Comparator<T> build() {
		return (a1, a2) -> {
			for (Comparator<T> comparator: comparatorList) {
				int cmp = comparator.compare(a1, a2);
				if (cmp != 0) {
					return cmp;
				}
			}
			return 0;
		};
	}
}
