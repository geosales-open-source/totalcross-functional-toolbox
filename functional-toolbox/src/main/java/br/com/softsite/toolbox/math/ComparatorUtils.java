package br.com.softsite.toolbox.math;

import java.util.Comparator;

public class ComparatorUtils {

	private ComparatorUtils() {}

	private static <V> Comparator<V> nullPos(Comparator<V> cmp, int nullposition) {
		return (a, b) -> a == b? 0:
				a == null? nullposition:
				b == null? -nullposition:
				cmp.compare(a, b);
	}

	public static <V> Comparator<V> nullFirst(Comparator<V> cmp) {
		return nullPos(cmp, -1);
	}

	public static <V> Comparator<V> nullLast(Comparator<V> cmp) {
		return nullPos(cmp, +1);
	}

	private static <V extends Comparable> Comparator<V> nullPos(int nullposition) {
		return (a, b) -> a == b? 0:
				a == null? nullposition:
				b == null? -nullposition:
				a.compareTo(b);
	}

	private static final Comparator<Comparable> NULL_FIRST = nullPos(-1);
	private static final Comparator<Comparable> NULL_LAST = nullPos(+1);

	public static <V extends Comparable> Comparator<V> nullFirst() {
		return (Comparator<V>) NULL_FIRST;
	}

	public static <V extends Comparable> Comparator<V> nullLast() {
		return (Comparator<V>) NULL_LAST;
	}

	public static <V> Comparator<V> reversed(Comparator<V> cmp) {
		return (a, b) -> cmp.compare(b, a);
	}
}