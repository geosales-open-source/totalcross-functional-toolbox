package br.com.softsite.toolbox.stream;

interface DoubleIterator {
	boolean hasNext();
	double next();
}
