package br.com.softsite.toolbox.collection;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

class ImmutableMap<K, V> implements Map<K, V> {

	private final Map<K, V> underlying;

	ImmutableMap(Map<K, V> underlying) {
		this.underlying = underlying;
	}

	@Override
	public int size() {
		return underlying.size();
	}

	@Override
	public boolean isEmpty() {
		return underlying.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return underlying.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return underlying.containsValue(value);
	}

	@Override
	public V get(Object key) {
		return underlying.get(key);
	}

	@Override
	public V put(K key, V value) {
		throw new UnsupportedOperationException("ImmutableMap cannot alter its elements");
	}

	@Override
	public V remove(Object key) {
		throw new UnsupportedOperationException("ImmutableMap cannot alter its elements");
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		throw new UnsupportedOperationException("ImmutableMap cannot alter its elements");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("ImmutableMap cannot alter its elements");
	}

	@Override
	public Set<K> keySet() {
		return underlying.keySet();
	}

	@Override
	public Collection<V> values() {
		return CollectionUtils.toImmutableCollection(underlying.values());
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		return CollectionUtils.toImmutableSet(underlying.entrySet());
	}
}
