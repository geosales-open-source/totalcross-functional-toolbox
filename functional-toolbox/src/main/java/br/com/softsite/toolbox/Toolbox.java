package br.com.softsite.toolbox;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;

public final class Toolbox {

	private Toolbox() {
	}
	
	public static <T> boolean isNotNullNorEmpty(ArrayList<T> l) {
		return l != null && !l.isEmpty();
	}

	public static <O, P> P getIfNotNull(O origem, Function<O, P> metodo) {
		return getIfNotNull(origem, metodo, null);
	}
	
	public static <O, P> P getIfNotNull(O origem, Function<O, P> metodo, P nullDefault) {
		if (origem != null) {
			return metodo.apply(origem);
		} else {
			return nullDefault;
		}
	}
	
	public static <O, P> P getIfNotNullLazyDefault(O origem, Function<O, P> metodo, Supplier<P> nullDefault) {
		if (origem != null) {
			return metodo.apply(origem);
		} else {
			return nullDefault.get();
		}
	}
	
	public static <T> T elvisLazy(Supplier<T> val, Supplier<T> defaultVal) {
		T v = val.get();
		if (v != null) {
			return v;
		}
		return defaultVal.get();
	}
	
	public static <T> boolean equals(T lhs, T rhs) {
		if (lhs == null) {
			return rhs == null;
		}
		
		return lhs.equals(rhs);
	}
	
	public static <T extends HasKey<?>> boolean equalsKey(T lhs, T rhs) {
		return equalsKey(lhs, rhs, HasKey::getKey);
	}
	
	public static <T, K> boolean equalsKey(T lhs, T rhs, Function<T, K> getKey) {
		if (lhs == null || rhs == null) {
			return lhs == rhs;
		}
		
		K lhsKey = getKey.apply(lhs);
		K rhsKey = getKey.apply(rhs);
		
		return equals(lhsKey, rhsKey);
	}

	public static <T> T elvis(T val, T defaultVal) {
		if (val != null) {
			return val;
		}
		return defaultVal;
	}
	
	/**
	 * criado de um jeito tal que, sempre que possível (caso mais simples do elvis),
	 * o primeiro é executado. O segundo só é executado em última instância, quando
	 * tem 3+ elementos
	 */
	@SafeVarargs
	public static <T> T elvis(T firstVal, T secondVal, T thirdVal, T... otherVals) {
		IntFunction<T> getElement = (i) -> {
			switch (i) {
			case 0:
				return firstVal;
			case 1:
				return secondVal;
			case 2:
				return thirdVal;
			default:
				return otherVals[i - 3];
			}
		};
		for (int i = 0; i < otherVals.length + 3; i++) {
			T candidate = getElement.apply(i);
			if (candidate != null) {
				return candidate;
			}
		}
		return null;
	}

	public static <T> boolean getIfNotNullPredicate(T origem, Predicate<T> predicado) {
		return origem != null? predicado.test(origem): false;
	}

	public static <O> void runIfNotNull(O origem, Consumer<O> method) {
		if (origem != null) {
			method.accept(origem);
		}
	}
}
