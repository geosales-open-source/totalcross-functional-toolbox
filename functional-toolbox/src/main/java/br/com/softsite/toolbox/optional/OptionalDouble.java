package br.com.softsite.toolbox.optional;

import java.util.NoSuchElementException;
import java.util.function.DoubleConsumer;
import java.util.function.DoubleSupplier;
import java.util.function.Supplier;

import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.DoubleStream;

public final class OptionalDouble {
	private static OptionalDouble empty = new OptionalDouble();
	
	private boolean isEmpty;
	private double t;

	private OptionalDouble() {
		this.isEmpty = true;
	}
	
	private OptionalDouble(double t) {
		this.isEmpty = false;
		this.t = t;
	}
	
	public void orElseThrow() {
		orElseThrow(() -> new NoSuchElementException("Empty optional"));
	}
	
	public <X extends Throwable> void orElseThrow(Supplier<X> exceptionSupplier) throws X {
		if (isEmpty) {
			throw exceptionSupplier.get();
		}
	}
	
	public double getAsDouble() {
		if (isEmpty) {
			throw new NoSuchElementException("Empty optional");
		}
		
		return t;
	}
	
	public void ifPresent(DoubleConsumer action) {
		ifPresentOrElse(action, FunctionUtils.noop());
	}
	
	public void ifPresentOrElse(DoubleConsumer action, Runnable emptyAction) {
		if (!isEmpty) {
			action.accept(t);
		} else {
			emptyAction.run();
		}
	}
	
	public boolean isEmpty() {
		return isEmpty;
	}
	
	public boolean isPresent() {
		return !isEmpty;
	}
	
	public double orElseGet(DoubleSupplier orF) {
		return isEmpty? orF.getAsDouble(): t;
	}
	
	public double orElse(double orT) {
		return isEmpty? orT: t;
	}
	
	public DoubleStream stream() {
		return isEmpty? DoubleStream.empty(): DoubleStream.of(t);
	}
	
	@Override
	public String toString() {
		return isEmpty? "Empty int optional": "Non-empty int optional: " + t;
	}
	
	@Override
	public int hashCode() {
		return isEmpty? 0: (int) t;
	}
	
	public static OptionalDouble of(double t) {
		return new OptionalDouble(t);
	}
	
	public static OptionalDouble empty() {
		return empty;
	}
}
