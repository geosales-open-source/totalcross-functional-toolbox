package br.com.softsite.toolbox.indirection;

public class BooleanIndirection {
	public boolean x;
	
	public BooleanIndirection() {
	}
	
	public BooleanIndirection(boolean x) {
		this.x = x;
	}

	public boolean getX() {
		return x;
	}
}
