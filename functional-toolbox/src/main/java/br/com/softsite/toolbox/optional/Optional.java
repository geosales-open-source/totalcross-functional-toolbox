package br.com.softsite.toolbox.optional;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.Stream;

public final class Optional<T> {
	private static Optional<?> empty = new Optional<>();
	
	private boolean isEmpty;
	private T t;

	private Optional() {
		this.isEmpty = true;
	}
	
	private Optional(T t) {
		this.isEmpty = false;
		this.t = t;
	}
	
	public void orElseThrow() {
		orElseThrow(() -> new NoSuchElementException("Empty optional"));
	}
	
	public <X extends Throwable> void orElseThrow(Supplier<X> exceptionSupplier) throws X {
		if (isEmpty) {
			throw exceptionSupplier.get();
		}
	}
	
	public Optional<T> filter(Predicate<T> test) {
		if (isEmpty) {
			return this;
		}
		
		if (test.test(t)) {
			return this;
		} else {
			return empty();
		}
	}
	
	public <U> Optional<U> flatMap(Function<T, Optional<U>> transform) {
		if (isEmpty) {
			return empty();
		}
		return transform.apply(t);
	}
	
	public <U> Optional<U> map(Function<T, U> transform) {
		return flatMap(x -> Optional.ofNullable(transform.apply(x)));
	}
	
	public T get() {
		if (isEmpty) {
			throw new NoSuchElementException("Empty optional");
		}
		
		return t;
	}
	
	public void ifPresent(Consumer<T> action) {
		ifPresentOrElse(action, FunctionUtils.noop());
	}
	
	public void ifPresentOrElse(Consumer<T> action, Runnable emptyAction) {
		if (!isEmpty) {
			action.accept(t);
		} else {
			emptyAction.run();
		}
	}
	
	public boolean isEmpty() {
		return isEmpty;
	}
	
	public boolean isPresent() {
		return !isEmpty;
	}
	
	public Optional<T> or(Supplier<Optional<T>> orF) {
		return isEmpty? orF.get(): this;
	}
	
	public T orElseGet(Supplier<T> orF) {
		return isEmpty? orF.get(): t;
	}
	
	public T orElse(T orT) {
		return isEmpty? orT: t;
	}
	
	public Stream<T> stream() {
		return isEmpty? Stream.empty(): Stream.of(t);
	}
	
	@Override
	public String toString() {
		return isEmpty? "Empty optional": "Non-empty optional: " + t.toString();
	}
	
	@Override
	public int hashCode() {
		return isEmpty? 0: t.hashCode();
	}
	
	public static <T> Optional<T> of(T t) {
		if (t == null) {
			throw new NullPointerException("Optional.of does not accept nulls");
		}
		return new Optional<>(t);
	}
	
	public static <T> Optional<T> ofNullable(T t) {
		return Toolbox.getIfNotNullLazyDefault(t, Optional<T>::new, Optional::empty);
	}

	@SuppressWarnings("unchecked")
	public static <T> Optional<T> empty() {
		return (Optional<T>) empty;
	}
}
