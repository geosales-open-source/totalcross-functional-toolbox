package br.com.softsite.toolbox.stream;

interface IntIterator {
	boolean hasNext();
	int next();
}
