package br.com.softsite.toolbox.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;

public final class CollectionUtils {
	private CollectionUtils() {}

	/**
	 * 
	 * @param element
	 * @return A mutable list with the passed element
	 */
	public static <T> List<T> toMutableSingleList(T element) {
		List<T> l = new ArrayList<>();
		l.add(element);
		return l;
	}

	/**
	 * 
	 * @param element
	 * @return As immutable list with only the passed element
	 */
	public static <T> List<T> toSingletonList(T element) {
		return toImmutableList(toMutableSingleList(element));
	}

	public static <T, C extends Collection<T>> C addAll(C baseElements, C otherElements) {
		baseElements.addAll(otherElements);

		return baseElements;
	}

	private static <X, C extends Collection<X>> C getCollection(C colecaoDesejada, Collection<X> dadosOriginais) {
		if (dadosOriginais != null) {
			colecaoDesejada.addAll(dadosOriginais);
		}

		return colecaoDesejada;
	}

	public static <X> HashSet<X> getHashSet(Collection<X> dadosOriginais) {
		if (dadosOriginais instanceof HashSet) {
			return (HashSet<X>) dadosOriginais;
		}
		return getCollection(new HashSet<>(dadosOriginais.size()), dadosOriginais);
	}

	public static <X> ArrayList<X> getArrayList(Collection<X> dadosOriginais) {
		if (dadosOriginais instanceof ArrayList) {
			return (ArrayList<X>) dadosOriginais;
		}
		return getCollection(new ArrayList<>(dadosOriginais.size()), dadosOriginais);
	}

	private static <X, C extends Collection<X>> C getCollection(C colecaoDesejada, Iterable<X> dadosOriginais) {
		if (dadosOriginais != null) {
			for (X dado: dadosOriginais) {
				colecaoDesejada.add(dado);
			}
		}

		return colecaoDesejada;
	}

	public static <X> HashSet<X> getHashSet(Iterable<X> dadosOriginais) {
		if (dadosOriginais instanceof HashSet) {
			return (HashSet<X>) dadosOriginais;
		}
		return getCollection(new HashSet<>(), dadosOriginais);
	}

	public static <X> ArrayList<X> getArrayList(Iterable<X> dadosOriginais) {
		if (dadosOriginais instanceof ArrayList) {
			return (ArrayList<X>) dadosOriginais;
		}
		return getCollection(new ArrayList<>(), dadosOriginais);
	}

	public static <K, V> HashMap<K, V> getHashMap(Map<K, V> mapOriginal) {
		if (mapOriginal instanceof HashMap) {
			return (HashMap<K, V>) mapOriginal;
		}
		return new Stream<>(mapOriginal.entrySet()).collect(Collectors.toMap(Entry::getKey, Entry::getValue, (v1, v2) -> v1, HashMap::new));
	}

	@SafeVarargs
	public static <T> List<T> toList(T... objs) {
		List<T> l = new ArrayList<T>(objs.length);

		for (T obj: objs) {
			l.add(obj);
		}
		return l;
	}

	public static <T> List<T> toImmutableList(List<T> l) {
		return new ImmutableList<>(l);
	}

	public static <T> Set<T> toImmutableSet(Set<T> l) {
		return new ImmutableSet<>(l);
	}

	public static <K, V> Map<K, V> toImmutableMap(Map<K, V> m) {
		return new ImmutableMap<>(m);
	}

	public static <T> Collection<T> toImmutableCollection(Collection<T> l) {
		return new ImmutableCollection<>(l);
	}
}
