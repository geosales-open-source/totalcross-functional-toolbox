package br.com.softsite.toolbox.stream;

interface DoubleIterable {
	DoubleIterator iterator();
}
