package br.com.softsite.toolbox.collection;

import java.util.Collection;
import java.util.Iterator;

class ImmutableCollection<T> implements Collection<T> {

	private final Collection<T> underlying;

	ImmutableCollection(Collection<T> underlying) {
		this.underlying = underlying;
	}

	@Override
	public int size() {
		return underlying.size();
	}

	@Override
	public boolean isEmpty() {
		return underlying.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return underlying.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		return new ImmutableIterator<>(underlying.iterator());
	}

	@Override
	public Object[] toArray() {
		return underlying.toArray();
	}

	@Override
	public <V> V[] toArray(V[] a) {
		return underlying.toArray(a);
	}

	@Override
	public boolean add(T e) {
		throw new UnsupportedOperationException("ImmutableCollection cannot alter its elements");
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("ImmutableCollection cannot alter its elements");
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return underlying.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		throw new UnsupportedOperationException("ImmutableCollection cannot alter its elements");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("ImmutableCollection cannot alter its elements");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("ImmutableCollection cannot alter its elements");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("ImmutableCollection cannot alter its elements");
	}
}
