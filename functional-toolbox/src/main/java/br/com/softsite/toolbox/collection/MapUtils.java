package br.com.softsite.toolbox.collection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import br.com.softsite.toolbox.HasKey;
import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;

public final class MapUtils {
	private MapUtils() {}

	public static <K, V> void addElement(Map<K, List<V>> map, K k, V v) {
		compute(map, k, (key, l) -> {
			if (l == null) {
				l = CollectionUtils.toMutableSingleList(v);
			} else {
				l.add(v);
			}
			return l;
		});
	}

	public static <K, V, T> void accumulate(Map<K, V> m, K key, T t, Function<T, V> toValue, BiFunction<V, V, V> mergeValues) {
		merge(m, key, toValue.apply(t), mergeValues);
	}

	public static <K, T extends HasKey<K>> HashMap<K, T> getMapFromKey(Iterable<T> elements) {
		return getMapFromKey(elements, HasKey::getKey);
	}

	public static <K, T> HashMap<K, T> getMapFromKey(Iterable<? extends T> elements, Function<T, K> getKey) {
		return new Stream<>(elements).collect(Collectors.toMap(getKey, FunctionUtils::identity, (a, b) -> {
			throw new IllegalStateException("can't merge");
		}, HashMap::new));
	}

	public static <K, V> V compute(Map<K, V> map, K k, BiFunction<K, V, V> remapping) {
		V oldValue = map.get(k);
		V newValue = remapping.apply(k, oldValue);

		if (newValue != oldValue) {
			if (newValue != null) {
				map.put(k, newValue);
			} else {
				map.remove(k);
			}
		}

		return newValue;
	 	}

	public static <K, V> V computeIfAbsent(Map<K, V> map, K k, Function<K, V> mapping) {
		V oldValue = map.get(k);
		if (oldValue == null) {
			V newValue = mapping.apply(k);
			if (newValue != null) {
				map.put(k, newValue);
			}
			return newValue;
		}

		return oldValue;
	}

	public static <K, V> V computeIfPresent(Map<K, V> map, K k, BiFunction<K, V, V> remapping) {
		V oldValue = map.get(k);
		if (oldValue != null) {
			V newValue = remapping.apply(k, oldValue);

			if (newValue != oldValue) {
				if (newValue != null) {
					map.put(k, newValue);
				} else {
					map.remove(k);
				}
			}
			return newValue;
		}

		return null;
	}

	public static <K, V> V getOrDefault(Map<K, V> map, K k, V defaultVal) {
		return Toolbox.elvis(map.get(k), defaultVal);
	}

	public static <K, V> V merge(Map<K, V> map, K k, V v, BiFunction<V, V, V> mergeFunction) {
		V oldValue = map.get(k);

		if (oldValue == null) {
			map.put(k, v);
			return v;
		} else {
			V newValue = mergeFunction.apply(oldValue, v);
			if (newValue == null) {
				map.remove(k);
			} else if (newValue != oldValue) {
				map.put(k, newValue);
			}
			return newValue;
		}
	}

	public static <K, V> V putIfAbsent(Map<K, V> map, K k, V v) {
		V oldValue = map.get(k);
		if (oldValue != null) {
			return oldValue;
		}
		map.put(k, v);
		return null;
	}

	public static <K, V> boolean remove(Map<K, V> map, K k, V v) {
		V oldValue = map.get(k);
		if (oldValue != null && oldValue.equals(v)) {
			map.remove(k);
			return true;
		}
		return false;
	}

	public static <K, V> V replace(Map<K, V> map, K k, V v) {
		if (map.containsKey(k)) {
			return map.put(k, v);
		}

		return null;
	}

	public static <K, V> boolean replace(Map<K, V> map, K k, V oldValue, V newValue) {
		if (map.containsKey(k) && Toolbox.equals(map.get(k), oldValue)) {
			map.put(k, newValue);
			return true;
		}

		return false;
	}

	public static <K, V> void replaceAll(Map<K, V> map, BiFunction<K, V, V> remapper) {
		new Stream<>(map.entrySet())
			.forEach(es -> es.setValue(remapper.apply(es.getKey(), es.getValue())));
	}
}
