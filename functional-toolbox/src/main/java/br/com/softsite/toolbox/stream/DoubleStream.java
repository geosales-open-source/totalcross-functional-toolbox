package br.com.softsite.toolbox.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiConsumer;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleConsumer;
import java.util.function.DoubleFunction;
import java.util.function.DoublePredicate;
import java.util.function.DoubleSupplier;
import java.util.function.DoubleToIntFunction;
import java.util.function.DoubleToLongFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.ObjDoubleConsumer;
import java.util.function.Supplier;

import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.indirection.DoubleIndirection;
import br.com.softsite.toolbox.indirection.Indirection;
import br.com.softsite.toolbox.indirection.IntIndirection;
import br.com.softsite.toolbox.indirection.LongIndirection;
import br.com.softsite.toolbox.math.DoubleSummaryStatistics;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.optional.OptionalDouble;

public class DoubleStream implements BaseStream<Double, DoubleStream> {
	DoubleIterable iterable;
	SimpleCloser closes;
	public DoubleStream(DoubleIterable iterable) {
		this(SimpleCloser.EMPTY, iterable);
	}

	DoubleStream(SimpleCloser closes, DoubleIterable iterable) {
		this.iterable = iterable;
		this.closes = closes;
	}

	// intermediate operations

	@Override
	public DoubleStream onClose(Runnable closeHandler) {
		closes = new SimpleCloser(closeHandler, closes);
		return this;
	}

	public DoubleStream filter(DoublePredicate filter) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			DoubleIterator iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			double valorFetch;

			void fetchNext() {
				firstRun = false;

				while (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (filter.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public double next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				double valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public <K> Stream<K> mapToObj(DoubleFunction<K> map) {
		return new Stream<>(closes, () -> new Iterator<K>() {
			DoubleIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public K next() {
				return map.apply(iterator.next());
			}
		});
	}

	public IntStream mapToInt(DoubleToIntFunction map) {
		return new IntStream(closes, () -> new IntIterator() {
			DoubleIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public int next() {
				return map.applyAsInt(iterator.next());
			}
		});
	}

	public LongStream mapToLong(DoubleToLongFunction map) {
		return new LongStream(closes, () -> new LongIterator() {
			DoubleIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public long next() {
				return map.applyAsLong(iterator.next());
			}
		});
	}

	public DoubleStream map(DoubleUnaryOperator map) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			DoubleIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public double next() {
				return map.applyAsDouble(iterator.next());
			}
		});
	}

	public DoubleStream flatMap(DoubleFunction<DoubleStream> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new DoubleStream(closes, () -> new DoubleIterator() {
			DoubleIterator iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			double valorFetch;
			DoubleIterator iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						DoubleStream stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public double next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				double valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public DoubleStream distinct() {
		// não tem a otimização possível de se fazer se for uma stream ordenada
		HashSet<Double> x = new HashSet<>();
		return filter(x::add);
	}

	public DoubleStream sorted() {
		ArrayList<Double> l = boxed().collect(Collectors.toCollection(ArrayList::new));
		Collections.sort(l, Double::compare);
		return new DoubleStream(closes, DoubleIterableUtils.toDoubleIterable(l));
	}

	public DoubleStream peek(DoubleConsumer peeker) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			DoubleIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public double next() {
				double element = iterator.next();
				peeker.accept(element);
				return element;
			}
		});
	}

	public DoubleStream limit(long max) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			long x = 0;
			DoubleIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				if (x < max) {
					x++;
					return iterator.hasNext();
				} else {
					return false;
				}
			}

			@Override
			public double next() {
				return iterator.next();
			}
		});
	}

	public DoubleStream skip(long n) {
		LongIndirection limit = new LongIndirection(0);
		return filter(t -> {
			if (limit.x < n) {
				limit.x++;
				return false;
			}
			return true;
		});
	}

	public DoubleStream takeWhile(DoublePredicate test) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			DoubleIterator iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			double valorFetch;

			void fetchNext() {
				firstRun = false;

				if (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (test.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					} else {
						conseguiuFetch = false;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public double next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				double valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public DoubleStream dropWhile(DoublePredicate test) {
		return takeWhile(FunctionUtils.negateDoublePredicate(test));
	}

	// terminal operations

	public Stream<Double> boxed() {
		return new Stream<>(closes, getIterable());
	}

	public void forEach(DoubleConsumer consumer) {
		for (double element: getIterable()) {
			consumer.accept(element);
		}
	}

	public double[] toArray() {
		double []array = new double[(int) count()];
		IntIndirection i = new IntIndirection(0);

		forEach(t -> {
			array[i.x] = t;
			i.x++;
		});

		return array;
	}

	public double reduce(int neutralElement, DoubleBinaryOperator accumulator) {
		DoubleIndirection acc = new DoubleIndirection(neutralElement);

		forEach(t -> {
			acc.x = accumulator.applyAsDouble(acc.x, t);
		});

		return acc.x;
	}

	public OptionalDouble reduce(DoubleBinaryOperator accumulator) {
		Indirection<OptionalDouble> x = new Indirection<>(OptionalDouble.empty());

		forEach(t -> x.x.ifPresentOrElse(t2 -> x.x = OptionalDouble.of(accumulator.applyAsDouble(t2, t)), () -> x.x = OptionalDouble.of(t)));

		return x.x;
	}

	public long count() {
		LongIndirection i = new LongIndirection(0L);

		forEach(t -> {
			i.x++;
		});

		return i.x;
	}

	public <R> R collect(Supplier<R> supplier, ObjDoubleConsumer<R> accumulator, BiConsumer<R, R> combiner) {
		R result = supplier.get();

		for (double t: getIterable()) {
			accumulator.accept(result, t);
		}

		return result;
	}

	public OptionalDouble max() {
		Double max = null;

		for (double x: getIterable()) {
			if (max == null) {
				max = x;
			} else if (x > max) {
				max = x;
			}
		}
		return Toolbox.getIfNotNullLazyDefault(max, OptionalDouble::of, OptionalDouble::empty);
	}

	public OptionalDouble min() {
		Double min = null;

		for (double x: getIterable()) {
			if (min == null) {
				min = x;
			} else if (x < min) {
				min = x;
			}
		}
		return Toolbox.getIfNotNullLazyDefault(min, OptionalDouble::of, OptionalDouble::empty);
	}

	public boolean anyMatch(DoublePredicate matcher) {
		for (double t: getIterable()) {
			if (matcher.test(t)) {
				return true;
			}
		}
		return false;
	}

	public boolean allMatch(DoublePredicate matcher) {
		for (double t: getIterable()) {
			if (!matcher.test(t)) {
				return false;
			}
		}
		return true;
	}

	public boolean noneMatch(DoublePredicate matcher) {
		return !anyMatch(matcher);
	}

	public OptionalDouble findFirst() {
		for (double t: getIterable()) {
			return OptionalDouble.of(t);
		}
		return OptionalDouble.empty();
	}

	public OptionalDouble findAny() {
		return findFirst();
	}

	public IntStream asIntStream() {
		return this.mapToInt(i -> (int) i);
	}

	public LongStream asLongStream() {
		return this.mapToLong(i -> (long) i);
	}

	public OptionalDouble average() {
		DoubleSummaryStatistics statistics = summaryStatistics();
		return statistics.getCount() == 0? OptionalDouble.empty(): OptionalDouble.of(statistics.getAverage());
	}

	public double sum() {
		double s = 0;
		for (double l: getIterable()) {
			s += l;
		}
		return s;
	}

	public DoubleSummaryStatistics summaryStatistics() {
		return collect(DoubleSummaryStatistics::new, DoubleSummaryStatistics::accept, DoubleSummaryStatistics::combine);
	}

	@Override
	public Iterator<Double> iterator() {
		return getIterable().iterator();
	}

	@Override
	public void close() {
		closes.close();
	}

	// static methods et al

	public static DoubleStream iterate(double seed, DoublePredicate hasNext, DoubleUnaryOperator next) {
		return new DoubleStream(() -> new DoubleIterator() {
			boolean conseguiuFetch = true;
			double valorFetch = seed;

			void fetchNext() {
				conseguiuFetch = hasNext.test(valorFetch);
				valorFetch = next.applyAsDouble(valorFetch);
			}

			@Override
			public boolean hasNext() {
				return conseguiuFetch;
			}

			@Override
			public double next() {
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				double valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public static DoubleStream iterate(double seed, DoubleUnaryOperator next) {
		return iterate(seed, x -> true, next);
	}

	public static DoubleStream generate(DoubleSupplier sup) {
		return iterate(sup.getAsDouble(), t -> sup.getAsDouble());
	}

	private static final DoubleStream emptyStream = new DoubleStream(() -> new DoubleIterator() {
		@Override
		public boolean hasNext() {
			return false;
		}

		@Override
		public double next() {
			throw new NoSuchElementException("Empty stream");
		}
	}) {
		@Override
		public DoubleStream onClose(Runnable closeHandler) {
			return new DoubleStream(new SimpleCloser(closeHandler), iterable);
		}
	};

	public static DoubleStream empty() {
		return emptyStream;
	}

	public static DoubleStream of(double t) {
		return new DoubleStream(DoubleIterableUtils.toDoubleIterable(t));
	}

	public static DoubleStream of(double... t) {
		return new DoubleStream(DoubleIterableUtils.toDoubleIterable(t));
	}

	public static DoubleStream concat(DoubleStream stream1, DoubleStream stream2) {
		return Stream.of(stream1.boxed(), stream2.boxed()).<Double>flatMap(FunctionUtils::identity).mapToDouble(Double::doubleValue).onClose(SimpleCloser.compose(stream1.closes, stream2.closes));
	}

	public static Builder builder() {
		return new Builder() {
			boolean builtState = false;
			ArrayList<Double> array = new ArrayList<>();

			private void ensureCanGo() {
				if (builtState) {
					throw new IllegalStateException("builder already in terminal state");
				}
			}

			@Override
			public void accept(double t) {
				ensureCanGo();
				array.add(t);
			}

			@Override
			public Builder add(double t) {
				accept(t);
				return this;
			}

			@Override
			public DoubleStream build() {
				ensureCanGo();
				builtState = true;
				return new DoubleStream(DoubleIterableUtils.toDoubleIterable(array));
			}
		};
	}

	public static interface Builder extends DoubleConsumer {
		Builder add(double t);
		DoubleStream build();
	}

	// private aux methods

	private Iterable<Double> getIterable() {
		return DoubleIterableUtils.toIterable(iterable);
	}
}
