package br.com.softsite.toolbox.indirection;

public class LongIndirection {
	public long x;
	
	public LongIndirection() {
	}
	
	public LongIndirection(long x) {
		this.x = x;
	}

	public long getX() {
		return x;
	}
}
