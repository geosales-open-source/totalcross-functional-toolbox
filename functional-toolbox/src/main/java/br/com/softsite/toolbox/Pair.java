package br.com.softsite.toolbox;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.Collector;
import br.com.softsite.toolbox.stream.Collectors;

public class Pair<K, V> implements IsSerializable {

	public static <V> V getValueOrNull(Pair<Boolean, V> pair) {
		return pair.key? pair.value: null;
	}

	public static <K,V> Function<K, V> toInjectiveFunction(Collection<Pair<K,V>> mapping) {
		return FunctionUtils.toInjectiveFunction(mapping, Pair::getKey, Pair::getValue);
	}

	private K key;
	private V value;

	protected Pair() {
	}

	public static <X, Y> Pair<X,Y> getPair(X x, Y y) {
		return new Pair<X, Y>(x, y);
	}

	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		return (key != null? key.hashCode(): 0) ^ (value != null? value.hashCode(): 0);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		} else if (other == this) {
			return true;
		} else if (!(other instanceof Pair)) {
			return false;
		}
		Pair<?,?> otherMyClass = (Pair<?,?>)other;

		return ((key == otherMyClass.key) || (key != null && key.equals(otherMyClass.key))) &&
				((value == otherMyClass.value) || (value != null && value.equals(otherMyClass.value)));
	}

	@Override
	public String toString() {
		return "<" + key + ";" + value + ">";
	}

	public static <T, A1, A2, K, V> Collector<T, ?, Pair<K, V>> toPairCollector(Collector<T, A1, K> collector1, Collector<T, A2, V> collector2) {
		return Collectors.<T, PairBuilder<A1, A2>, Pair<K, V>>of(() -> new PairBuilder<>(collector1.supplier().get(), collector2.supplier().get()), (pa, t) -> {
			collector1.accumulator().accept(pa.getKey(), t);
			collector2.accumulator().accept(pa.getValue(), t);
		}, (pa1, pa2) -> {
			pa1.setKey(collector1.combiner().apply(pa1.getKey(), pa2.getKey()));
			pa1.setValue(collector2.combiner().apply(pa1.getValue(), pa2.getValue()));
			return pa1;
		}, pa -> {
			return Pair.getPair(collector1.finisher().apply(pa.getKey()), collector2.finisher().apply(pa.getValue()));
		});
	}

	public static <K1, V1, K2, V2, A1, A2> Collector<Pair<K1, V1>, ?, Pair<K2, V2>> getPairCollector(Collector<K1, A1, K2> collector1, Collector<V1, A2, V2> collector2) {
		return Collectors.<Pair<K1, V1>, PairBuilder<A1, A2>, Pair<K2, V2>>of(() -> new PairBuilder<>(collector1.supplier().get(), collector2.supplier().get()), (pa, pt) -> {
			collector1.accumulator().accept(pa.getKey(), pt.key);
			collector2.accumulator().accept(pa.getValue(), pt.value);
		}, (pa1, pa2) -> {
			pa1.setKey(collector1.combiner().apply(pa1.getKey(), pa2.getKey()));
			pa1.setValue(collector2.combiner().apply(pa1.getValue(), pa2.getValue()));
			return pa1;
		}, pa -> {
			return Pair.getPair(collector1.finisher().apply(pa.getKey()), collector2.finisher().apply(pa.getValue()));
		});
	}

	public Pair<V, K> invert() {
		return Pair.getPair(getValue(), getKey());
	}

	@Deprecated
	public <T> T apllyFunction(BiFunction<K, V, T> func) {
		return applyFunction(func);
	}

	public <T> T applyFunction(BiFunction<K, V, T> func) {
		return func.apply(getKey(), getValue());
	}

	public void consume(BiConsumer<K, V> consumer) {
		consumer.accept(getKey(), getValue());
	}

	public void consume(Consumer<K> consumerKey, Consumer<V> consumerValue) {
		this.consume((k,v) -> {
			consumerKey.accept(k);
			consumerValue.accept(v);
		});
	}
}
