package br.com.softsite.toolbox.math;

import java.util.function.IntConsumer;
import java.util.function.LongConsumer;

public class LongSummaryStatistics implements IntConsumer, LongConsumer {
	private long min = Long.MAX_VALUE ;
	private long max = Long.MIN_VALUE ;
	private long count = 0;
	private long sum = 0;
	private boolean dirty = false;
	private double average;
	
	public void combine(LongSummaryStatistics another) {
		if (another.count != 0) {
			dirty = true;
			
			count += another.count;
			sum += another.sum;
			if (another.min < min) {
				min = another.min;
			}
			if (another.max > max) {
				max = another.max;
			}
			
		}
	}
	
	@Override
	public void accept(int value) {
		if (count != 0) {
			count++;
			sum += value;
			
			if (value < min) {
				min = value;
			} else if (value > max) {
				max = value;
			}
		} else {
			count = 1;
			sum = min = max = value;
		}
		dirty = true;
	}
	
	@Override
	public void accept(long value) {
		if (count != 0) {
			count++;
			sum += value;
			
			if (value < min) {
				min = value;
			} else if (value > max) {
				max = value;
			}
		} else {
			count = 1;
			sum = min = max = value;
		}
		dirty = true;
	}
	
	public long getMax() {
		return max;
	}
	
	public long getMin() {
		return min;
	}
	
	public long getCount() {
		return count;
	}
	
	public long getSum() {
		return sum;
	}
	
	public double getAverage() {
		if (dirty) {
			if (count == 0) {
				average = 0;
			} else {
				average = sum/((double) count);
			}
			dirty = false;
		}
		return average;
	}
}
