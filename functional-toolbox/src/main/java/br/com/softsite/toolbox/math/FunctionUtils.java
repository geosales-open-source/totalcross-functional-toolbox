package br.com.softsite.toolbox.math;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.DoublePredicate;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.LongPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;

import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;

public final class FunctionUtils {
	private FunctionUtils() {}
	
	public static Supplier<String> getStringSupplier(final String str) {
		return getSelfSupplier(str);
	}
	
	public static <T> Supplier<T> getSelfSupplier(final T obj) {
		return () -> obj;
	}
	
	public static Supplier<String> concatStringSupplier(String supplier1, String supplier2) {
		return concatStringSupplier(getStringSupplier(supplier1), getStringSupplier(supplier2));
	}
	
	public static Supplier<String> concatStringSupplier(Supplier<String> supplier1, String supplier2) {
		return concatStringSupplier(supplier1, getStringSupplier(supplier2));
	}
	
	public static Supplier<String> concatStringSupplier(String supplier1, Supplier<String> supplier2) {
		return concatStringSupplier(getStringSupplier(supplier1), supplier2);
	}
	
	public static Supplier<String> concatStringSupplier(final Supplier<String> supplier1, final Supplier<String> supplier2) {
		return () -> supplier1.get() + supplier2.get();
	}
	
	@SafeVarargs
	public static <T> Consumer<T> andThen(final Consumer<T>... consumers) {
		return (t) -> {
			for (Consumer<T> consumer: consumers) {
				consumer.accept(t);
			}
		};
	}
	
	public static <E,P,O> Function<E, Pair<P,O>> agregateFunction(final Function<E,P> toP, final Function<E,O> toO) {
		return (entrada) -> Pair.getPair(toP.apply(entrada), toO.apply(entrada));
	}
	
	public static <T> Predicate<T> negate(Predicate<T> affirmative) {
		return t -> !affirmative.test(t);
	}
	
	public static IntPredicate negateIntPredicate(IntPredicate affirmative) {
		return t -> !affirmative.test(t);
	}
	
	public static DoublePredicate negateDoublePredicate(DoublePredicate affirmative) {
		return t -> !affirmative.test(t);
	}
	
	public static LongPredicate negateLongPredicate(LongPredicate affirmative) {
		return t -> !affirmative.test(t);
	}
	
	public static <T> Predicate<T> functionTrue() {
		return t -> true;
	}
	
	public static <T> Predicate<T> functionIsNotNull() {
		return (entrada) -> entrada != null;
	}
	
	public static <T> Predicate<T> functionIsNotNull(Class<T> clazz) {
		return FunctionUtils.<T>functionIsNotNull();
	}
	
	public static <T, U, V> Function<T,V> compose(final Function<T, U> before, final Function<U, V> after) {
		return (entrada) -> after.apply(before.apply(entrada));
	}

	public static <T> Predicate<T> and(Predicate<T> t1, Predicate<T> t2) {
		return t -> t1.test(t) && t2.test(t);
	}
	
	public static<T, V, K> Function<K, V> toInjectiveFunction(Collection<T> domain, Function<T, K> keyGetter, Function<T, V> valueGetter) {
		return new Stream<>(domain).collect(Collectors.toMap(keyGetter, valueGetter))::get;
	}

	public static<T, V, K> Function<K, List<V>> toFunction(Collection<T> domain, Function<T, K> keyGetter, Function<T, V> valueGetter) {
		if (domain.isEmpty()) {
			return k -> new ArrayList<>();
		}

		Map<K, List<T>> valueMapper = new Stream<>(domain).collect(Collectors.groupingBy(keyGetter));
		return k -> Toolbox.<List<T>, Stream<T>>getIfNotNullLazyDefault(valueMapper.get(k), Stream<T>::new, Stream::empty).map(valueGetter).collect(Collectors.toList());
	}

	public static Runnable noop() {
		return () -> {};
	}

	public static <T> Consumer<T> noopConsumer() {
		return t -> {};
	}

	public static <T> T identity(T t) {
		return t;
	}
}
