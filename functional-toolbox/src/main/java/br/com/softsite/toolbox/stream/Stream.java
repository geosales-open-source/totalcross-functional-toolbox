package br.com.softsite.toolbox.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.indirection.Indirection;
import br.com.softsite.toolbox.indirection.IntIndirection;
import br.com.softsite.toolbox.indirection.LongIndirection;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.optional.Optional;

public class Stream<T> implements BaseStream<T, Stream<T>> {
	Iterable<T> iterable;
	SimpleCloser closes;
	public Stream(Iterable<T> iterable) {
		this(SimpleCloser.EMPTY, iterable);
	}

	Stream(SimpleCloser closes, Iterable<T> iterable) {
		this.iterable = iterable;
		this.closes = closes;
	}

	// intermediate operations

	@Override
	public Stream<T> onClose(Runnable closeHandler) {
		closes = new SimpleCloser(closeHandler, closes);
		return this;
	}

	public Stream<T> filter(Predicate<T> filter) {
		return new Stream<>(closes, () -> new Iterator<T>() {
			Iterator<T> iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			T valorFetch;

			void fetchNext() {
				firstRun = false;

				while (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (filter.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public T next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				T valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public <K> Stream<K> map(Function<T, K> map) {
		return new Stream<>(closes, () -> new Iterator<K>() {
			Iterator<T> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public K next() {
				return map.apply(iterator.next());
			}
		});
	}

	public IntStream mapToInt(ToIntFunction<T> map) {
		return new IntStream(closes, () -> new IntIterator() {
			Iterator<T> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public int next() {
				return map.applyAsInt(iterator.next());
			}
		});
	}

	public DoubleStream mapToDouble(ToDoubleFunction<T> map) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			Iterator<T> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public double next() {
				return map.applyAsDouble(iterator.next());
			}
		});
	}

	public LongStream mapToLong(ToLongFunction<T> map) {
		return new LongStream(closes, () -> new LongIterator() {
			Iterator<T> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public long next() {
				return map.applyAsLong(iterator.next());
			}
		});
	}

	public <K> Stream<K> flatMap(Function<T, Stream<K>> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new Stream<K>(closes, () -> new Iterator<K>() {
			Iterator<T> iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			K valorFetch;
			Iterator<K> iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						Stream<K> stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public K next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				K valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public IntStream flatMapToInt(Function<T, IntStream> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new IntStream(closes, () -> new IntIterator() {
			Iterator<T> iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			int valorFetch;
			IntIterator iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						IntStream stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public int next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				int valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public LongStream flatMapToLong(Function<T, LongStream> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new LongStream(closes, () -> new LongIterator() {
			Iterator<T> iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			long valorFetch;
			LongIterator iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						LongStream stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public long next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				long valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public DoubleStream flatMapToDouble(Function<T, DoubleStream> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new DoubleStream(closes, () -> new DoubleIterator() {
			Iterator<T> iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			double valorFetch;
			DoubleIterator iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						DoubleStream stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public double next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				double valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public Stream<T> distinct() {
		// não tem a otimização possível de se fazer se for uma stream ordenada
		HashSet<T> x = new HashSet<>();
		return filter(x::add);
	}

	public Stream<T> sorted(Comparator<T> comp) {
		ArrayList<T> l = collect(Collectors.toCollection(ArrayList::new));
		Collections.sort(l, comp);
		return new Stream<>(closes, l);
	}

	public Stream<T> peek(Consumer<T> peeker) {
		return new Stream<>(closes, () -> new Iterator<T>() {
			Iterator<T> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public T next() {
				T element = iterator.next();
				peeker.accept(element);
				return element;
			}
		});
	}

	public Stream<T> limit(long max) {
		return new Stream<>(closes, () -> new Iterator<T>() {
			long x = 0;
			Iterator<T> iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				if (x < max) {
					x++;
					return iterator.hasNext();
				} else {
					return false;
				}
			}

			@Override
			public T next() {
				return iterator.next();
			}
		});
	}

	public Stream<T> skip(long n) {
		LongIndirection limit = new LongIndirection(0);
		return filter(t -> {
			if (limit.x < n) {
				limit.x++;
				return false;
			}
			return true;
		});
	}

	public Stream<T> takeWhile(Predicate<T> test) {
		return new Stream<>(closes, () -> new Iterator<T>() {
			Iterator<T> iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			T valorFetch;

			void fetchNext() {
				firstRun = false;

				if (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (test.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					} else {
						conseguiuFetch = false;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public T next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				T valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public Stream<T> dropWhile(Predicate<T> test) {
		return takeWhile(FunctionUtils.negate(test));
	}

	// terminal operations

	public void forEach(Consumer<T> consumer) {
		for (T element: iterable) {
			consumer.accept(element);
		}
	}

	public Object[] toArray() {
		Object []array = new Object[(int) count()];
		IntIndirection i = new IntIndirection(0);

		forEach(t -> {
			array[i.x] = t;
			i.x++;
		});

		return array;
	}

	@SuppressWarnings("unchecked")
	public <A> A[] toArray(IntFunction<A[]> generator) {
		A []array = generator.apply((int) count());
		IntIndirection i = new IntIndirection(0);

		forEach(t -> {
			array[i.x] = (A) t;
			i.x++;
		});

		return array;
	}

	public T reduce(T neutralElement, BiFunction<T, T, T> accumulator) {
		Indirection<T> acc = new Indirection<>(neutralElement);

		forEach(t -> {
			acc.x = accumulator.apply(acc.x, t);
		});

		return acc.x;
	}

	public Optional<T> reduce(BiFunction<T, T, T> accumulator) {
		Indirection<Optional<T>> x = new Indirection<>(Optional.empty());

		forEach(t -> x.x.ifPresentOrElse(t2 -> x.x = Optional.of(accumulator.apply(t2, t)), () -> x.x = Optional.of(t)));

		return x.x;
	}

	public <U> U reduce(U neutralElement, BiFunction<U, T, U> accumulator, BiFunction<U, U, U> combiner) {
		Indirection<U> acc = new Indirection<>(neutralElement);

		forEach(t -> {
			acc.x = accumulator.apply(acc.x, t);
		});

		return acc.x;
	}


	public long count() {
		LongIndirection i = new LongIndirection(0L);

		forEach(t -> {
			i.x++;
		});

		return i.x;
	}

	public <R> R collect(Supplier<R> supplier, BiConsumer<R, T> accumulator, BiConsumer<R, R> combiner) {
		R result = supplier.get();

		for (T t: iterable) {
			accumulator.accept(result, t);
		}

		return result;
	}

	public <R, A> R collect(Collector<? super T, A, R> collector) {
		A result = collector.supplier().get();
		BiConsumer<A, ? super T> accumulator = collector.accumulator();

		for (T t: iterable) {
			accumulator.accept(result, t);
		}

		return collector.finisher().apply(result);
	}

	public Optional<T> min(Comparator<T> comparator) {
		return collect(Collectors.<T, Indirection<Optional<T>>, Optional<T>>of(() -> {
			return new Indirection<>(Optional.empty());
		}, (indMin, x) -> indMin.x.ifPresentOrElse((t) -> {
				if (comparator.compare(x, t) < 0) {
					indMin.x = Optional.of(x);
				}
			}, () -> {
				indMin.x = Optional.of(x);
			}),
		(indMin1, indMin2) -> {
			indMin1.x.ifPresentOrElse((op) -> {
				indMin2.x.ifPresent((op2) -> {
					if (comparator.compare(op2, op) < 0) {
						indMin1.x = indMin2.x;
					}
				});
			}, () -> indMin1.x = indMin2.x);
			return indMin1;
		}, Indirection::getX));
	}

	public Optional<T> max(Comparator<T> comparator) {
		return collect(Collectors.<T, Indirection<Optional<T>>, Optional<T>>of(() -> {
			return new Indirection<>(Optional.empty());
		}, (indMax, x) -> indMax.x.ifPresentOrElse((t) -> {
				if (comparator.compare(x, t) > 0) {
					indMax.x = Optional.of(x);
				}
			}, () -> {
				indMax.x = Optional.of(x);
			}),
		(indMax1, indMax2) -> {
			indMax1.x.ifPresentOrElse((op) -> {
				indMax2.x.ifPresent((op2) -> {
					if (comparator.compare(op2, op) > 0) {
						indMax1.x = indMax2.x;
					}
				});
			}, () -> indMax1.x = indMax2.x);
			return indMax1;
		}, Indirection::getX));
	}

	public boolean anyMatch(Predicate<T> matcher) {
		for (T t: iterable) {
			if (matcher.test(t)) {
				return true;
			}
		}
		return false;
	}

	public boolean allMatch(Predicate<T> matcher) {
		for (T t: iterable) {
			if (!matcher.test(t)) {
				return false;
			}
		}
		return true;
	}

	public boolean noneMatch(Predicate<T> matcher) {
		return !anyMatch(matcher);
	}

	public Optional<T> findFirst() {
		for (T t: iterable) {
			return Optional.of(t);
		}
		return Optional.empty();
	}

	public Optional<T> findAny() {
		return findFirst();
	}

	@Override
	public Iterator<T> iterator() {
		return iterable.iterator();
	}

	@Override
	public void close() {
		closes.close();
	}

	// static methods et al

	public static <T> Stream<T> iterate(T seed, Predicate<T> hasNext, Function<T, T> next) {
		return new Stream<>(() -> new Iterator<T>() {
			boolean conseguiuFetch = true;
			T valorFetch = seed;

			void fetchNext() {
				conseguiuFetch = hasNext.test(valorFetch);
				valorFetch = next.apply(valorFetch);
			}

			@Override
			public boolean hasNext() {
				return conseguiuFetch;
			}

			@Override
			public T next() {
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				T valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public static <T> Stream<T> iterate(T seed, Function<T, T> next) {
		return iterate(seed, FunctionUtils.functionTrue(), next);
	}

	public static <T> Stream<T> generate(Supplier<T> sup) {
		return iterate(sup.get(), t -> sup.get());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static final Stream emptyStream = new Stream<Object>(() -> new Iterator() {
		@Override
		public boolean hasNext() {
			return false;
		}

		@Override
		public Object next() {
			return null;
		}
	}) {
		@Override
		public Stream<Object> onClose(Runnable closeHandler) {
			return new Stream<Object>(new SimpleCloser(closeHandler), this.iterable);
		}
	};

	@SuppressWarnings("unchecked")
	public static <T> Stream<T> empty() {
		return emptyStream;
	}

	public static <T> Stream<T> of(T t) {
		return new Stream<>(CollectionUtils.toSingletonList(t));
	}

	@SafeVarargs
	public static <T> Stream<T> of(T... values) {
		return new Stream<>(CollectionUtils.toList(values));
	}

	public static <T> Stream<T> ofNullable(T t) {
		return Toolbox.getIfNotNull(t, Stream::of, empty());
	}

	public static <T> Stream<T> concat(Stream<T> stream1, Stream<T> stream2) {
		return Stream.of(stream1, stream2).<T>flatMap(FunctionUtils::identity).onClose(SimpleCloser.compose(stream1.closes, stream2.closes));
	}

	public static <T> Builder<T> builder() {
		return new Builder<T>() {
			boolean builtState = false;
			ArrayList<T> array = new ArrayList<>();

			private void ensureCanGo() {
				if (builtState) {
					throw new IllegalStateException("builder already in terminal state");
				}
			}

			@Override
			public void accept(T t) {
				ensureCanGo();
				array.add(t);
			}

			@Override
			public Builder<T> add(T t) {
				accept(t);
				return this;
			}

			@Override
			public Stream<T> build() {
				ensureCanGo();
				builtState = true;
				return new Stream<>(array);
			}
		};
	}

	public static interface Builder<T> extends Consumer<T> {
		Builder<T> add(T t);
		Stream<T> build();
	}

}
