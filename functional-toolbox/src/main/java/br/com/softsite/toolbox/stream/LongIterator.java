package br.com.softsite.toolbox.stream;

interface LongIterator {
	boolean hasNext();
	long next();
}
