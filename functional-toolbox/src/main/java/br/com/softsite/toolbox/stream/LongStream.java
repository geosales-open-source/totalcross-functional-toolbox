package br.com.softsite.toolbox.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiConsumer;
import java.util.function.LongBinaryOperator;
import java.util.function.LongConsumer;
import java.util.function.LongFunction;
import java.util.function.LongPredicate;
import java.util.function.LongSupplier;
import java.util.function.LongToDoubleFunction;
import java.util.function.LongToIntFunction;
import java.util.function.LongUnaryOperator;
import java.util.function.ObjLongConsumer;
import java.util.function.Supplier;

import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.indirection.Indirection;
import br.com.softsite.toolbox.indirection.IntIndirection;
import br.com.softsite.toolbox.indirection.LongIndirection;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.math.LongSummaryStatistics;
import br.com.softsite.toolbox.optional.OptionalDouble;
import br.com.softsite.toolbox.optional.OptionalLong;

public class LongStream implements BaseStream<Long, LongStream> {
	LongIterable iterable;
	SimpleCloser closes;
	public LongStream(LongIterable iterable) {
		this(SimpleCloser.EMPTY, iterable);
	}

	LongStream(SimpleCloser closes, LongIterable iterable) {
		this.iterable = iterable;
		this.closes = closes;
	}

	// intermediate operations

	@Override
	public LongStream onClose(Runnable closeHandler) {
		closes = new SimpleCloser(closeHandler, closes);
		return this;
	}

	public LongStream filter(LongPredicate filter) {
		return new LongStream(closes, () -> new LongIterator() {
			LongIterator iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			long valorFetch;

			void fetchNext() {
				firstRun = false;

				while (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (filter.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public long next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				long valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public <K> Stream<K> mapToObj(LongFunction<K> map) {
		return new Stream<>(closes, () -> new Iterator<K>() {
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public K next() {
				return map.apply(iterator.next());
			}
		});
	}

	public IntStream mapToInt(LongToIntFunction map) {
		return new IntStream(closes, () -> new IntIterator() {
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public int next() {
				return map.applyAsInt(iterator.next());
			}
		});
	}

	public DoubleStream mapToDouble(LongToDoubleFunction map) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public double next() {
				return map.applyAsDouble(iterator.next());
			}
		});
	}

	public LongStream map(LongUnaryOperator map) {
		return new LongStream(closes, () -> new LongIterator() {
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public long next() {
				return map.applyAsLong(iterator.next());
			}
		});
	}

	public LongStream flatMap(LongFunction<LongStream> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new LongStream(closes, () -> new LongIterator() {
			LongIterator iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			long valorFetch;
			LongIterator iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						LongStream stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public long next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				long valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public LongStream distinct() {
		// não tem a otimização possível de se fazer se for uma stream ordenada
		HashSet<Long> x = new HashSet<>();
		return filter(x::add);
	}

	public LongStream sorted() {
		ArrayList<Long> l = boxed().collect(Collectors.toCollection(ArrayList::new));
		Collections.sort(l, Long::compareTo);
		return new LongStream(closes, LongIterableUtils.toLongIterable(l));
	}

	public LongStream peek(LongConsumer peeker) {
		return new LongStream(closes, () -> new LongIterator() {
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public long next() {
				long element = iterator.next();
				peeker.accept(element);
				return element;
			}
		});
	}

	public LongStream limit(long max) {
		return new LongStream(closes, () -> new LongIterator() {
			long x = 0;
			LongIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				if (x < max) {
					x++;
					return iterator.hasNext();
				} else {
					return false;
				}
			}

			@Override
			public long next() {
				return iterator.next();
			}
		});
	}

	public LongStream skip(long n) {
		LongIndirection limit = new LongIndirection(0);
		return filter(t -> {
			if (limit.x < n) {
				limit.x++;
				return false;
			}
			return true;
		});
	}

	public LongStream takeWhile(LongPredicate test) {
		return new LongStream(closes, () -> new LongIterator() {
			LongIterator iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			long valorFetch;

			void fetchNext() {
				firstRun = false;

				if (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (test.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					} else {
						conseguiuFetch = false;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public long next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				long valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public LongStream dropWhile(LongPredicate test) {
		return takeWhile(FunctionUtils.negateLongPredicate(test));
	}

	// terminal operations

	public Stream<Long> boxed() {
		return new Stream<>(closes, getIterable());
	}

	public void forEach(LongConsumer consumer) {
		for (long element: getIterable()) {
			consumer.accept(element);
		}
	}

	public long[] toArray() {
		long []array = new long[(int) count()];
		IntIndirection i = new IntIndirection(0);

		forEach(t -> {
			array[i.x] = t;
			i.x++;
		});

		return array;
	}

	public long reduce(int neutralElement, LongBinaryOperator accumulator) {
		LongIndirection acc = new LongIndirection(neutralElement);

		forEach(t -> {
			acc.x = accumulator.applyAsLong(acc.x, t);
		});

		return acc.x;
	}

	public OptionalLong reduce(LongBinaryOperator accumulator) {
		Indirection<OptionalLong> x = new Indirection<>(OptionalLong.empty());

		forEach(t -> x.x.ifPresentOrElse(t2 -> x.x = OptionalLong.of(accumulator.applyAsLong(t2, t)), () -> x.x = OptionalLong.of(t)));

		return x.x;
	}

	public long count() {
		LongIndirection i = new LongIndirection(0L);

		forEach(t -> {
			i.x++;
		});

		return i.x;
	}

	public <R> R collect(Supplier<R> supplier, ObjLongConsumer<R> accumulator, BiConsumer<R, R> combiner) {
		R result = supplier.get();

		for (long t: getIterable()) {
			accumulator.accept(result, t);
		}

		return result;
	}

	public OptionalLong max() {
		Long max = null;

		for (long x: getIterable()) {
			if (max == null) {
				max = x;
			} else if (x > max) {
				max = x;
			}
		}
		return Toolbox.getIfNotNullLazyDefault(max, OptionalLong::of, OptionalLong::empty);
	}

	public OptionalLong min() {
		Long min = null;

		for (long x: getIterable()) {
			if (min == null) {
				min = x;
			} else if (x < min) {
				min = x;
			}
		}
		return Toolbox.getIfNotNullLazyDefault(min, OptionalLong::of, OptionalLong::empty);
	}

	public boolean anyMatch(LongPredicate matcher) {
		for (long t: getIterable()) {
			if (matcher.test(t)) {
				return true;
			}
		}
		return false;
	}

	public boolean allMatch(LongPredicate matcher) {
		for (long t: getIterable()) {
			if (!matcher.test(t)) {
				return false;
			}
		}
		return true;
	}

	public boolean noneMatch(LongPredicate matcher) {
		return !anyMatch(matcher);
	}

	public OptionalLong findFirst() {
		for (long t: getIterable()) {
			return OptionalLong.of(t);
		}
		return OptionalLong.empty();
	}

	public OptionalLong findAny() {
		return findFirst();
	}

	public IntStream asIntStream() {
		return this.mapToInt(i -> (int) i);
	}

	public DoubleStream asLongStream() {
		return this.mapToDouble(i -> (double) i);
	}

	public OptionalDouble average() {
		LongSummaryStatistics statistics = summaryStatistics();
		return statistics.getCount() == 0? OptionalDouble.empty(): OptionalDouble.of(statistics.getAverage());
	}

	public long sum() {
		long s = 0;
		for (long l: getIterable()) {
			s += l;
		}
		return s;
	}

	public LongSummaryStatistics summaryStatistics() {
		return collect(LongSummaryStatistics::new, LongSummaryStatistics::accept, LongSummaryStatistics::combine);
	}

	@Override
	public Iterator<Long> iterator() {
		return getIterable().iterator();
	}

	@Override
	public void close() {
		closes.close();
	}

	// static methods et al

	public static LongStream iterate(long seed, LongPredicate hasNext, LongUnaryOperator next) {
		return new LongStream(() -> new LongIterator() {
			boolean conseguiuFetch = true;
			long valorFetch = seed;

			void fetchNext() {
				conseguiuFetch = hasNext.test(valorFetch);
				valorFetch = next.applyAsLong(valorFetch);
			}

			@Override
			public boolean hasNext() {
				return conseguiuFetch;
			}

			@Override
			public long next() {
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				long valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public static LongStream iterate(long seed, LongUnaryOperator next) {
		return iterate(seed, x -> true, next);
	}

	public static LongStream generate(LongSupplier sup) {
		return iterate(sup.getAsLong(), t -> sup.getAsLong());
	}

	private static final LongStream emptyStream = new LongStream(() -> new LongIterator() {
		@Override
		public boolean hasNext() {
			return false;
		}

		@Override
		public long next() {
			throw new NoSuchElementException("Empty stream");
		}
	}) {
		@Override
		public LongStream onClose(Runnable closeHandler) {
			return new LongStream(new SimpleCloser(closeHandler), this.iterable);
		}
	};

	public static LongStream empty() {
		return emptyStream;
	}

	public static LongStream of(long t) {
		return new LongStream(LongIterableUtils.toLongIterable(t));
	}

	public static LongStream of(long... t) {
		return new LongStream(LongIterableUtils.toLongIterable(t));
	}

	public static LongStream range(long ini, long endExclusive) {
		return new LongStream(() -> new LongIterator() {
			long fetched = ini;
			@Override
			public long next() {
				if (!hasNext()) {
					throw new NoSuchElementException("End of range");
				}
				long atual = fetched;
				fetched++;
				return atual;
			}

			@Override
			public boolean hasNext() {
				return fetched < endExclusive;
			}
		});
	}

	public static LongStream rangeClosed(long ini, long endInclusive) {
		return range(ini, endInclusive + 1);
	}

	public static LongStream concat(LongStream stream1, LongStream stream2) {
		return Stream.of(stream1.boxed(), stream2.boxed()).<Long>flatMap(FunctionUtils::identity).mapToLong(Long::longValue).onClose(SimpleCloser.compose(stream1.closes, stream2.closes));
	}

	public static Builder builder() {
		return new Builder() {
			boolean builtState = false;
			ArrayList<Long> array = new ArrayList<>();

			private void ensureCanGo() {
				if (builtState) {
					throw new IllegalStateException("builder already in terminal state");
				}
			}

			@Override
			public void accept(long t) {
				ensureCanGo();
				array.add(t);
			}

			@Override
			public Builder add(long t) {
				accept(t);
				return this;
			}

			@Override
			public LongStream build() {
				ensureCanGo();
				builtState = true;
				return new LongStream(LongIterableUtils.toLongIterable(array));
			}
		};
	}

	public static interface Builder extends LongConsumer {
		Builder add(long t);
		LongStream build();
	}

	// private aux methods

	private Iterable<Long> getIterable() {
		return LongIterableUtils.toIterable(iterable);
	}
}
