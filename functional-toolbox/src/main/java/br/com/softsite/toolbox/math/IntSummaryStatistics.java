package br.com.softsite.toolbox.math;

import java.util.function.IntConsumer;

public class IntSummaryStatistics implements IntConsumer {
	private int min = Integer.MAX_VALUE;
	private int max = Integer.MIN_VALUE;
	private long count = 0;
	private long sum = 0;
	private boolean dirty = false;
	private double average;
	
	public void combine(IntSummaryStatistics another) {
		if (another.count != 0) {
			dirty = true;
			
			count += another.count;
			sum += another.sum;
			if (another.min < min) {
				min = another.min;
			}
			if (another.max > max) {
				max = another.max;
			}
			
		}
	}
	
	@Override
	public void accept(int value) {
		if (count != 0) {
			count++;
			sum += value;
			
			if (value < min) {
				min = value;
			} else if (value > max) {
				max = value;
			}
		} else {
			count = 1;
			sum = min = max = value;
		}
		dirty = true;
	}
	
	public int getMax() {
		return max;
	}
	
	public int getMin() {
		return min;
	}
	
	public long getCount() {
		return count;
	}
	
	public long getSum() {
		return sum;
	}
	
	public double getAverage() {
		if (dirty) {
			if (count == 0) {
				average = 0;
			} else {
				average = sum/((double) count);
			}
			dirty = false;
		}
		return average;
	}
}
