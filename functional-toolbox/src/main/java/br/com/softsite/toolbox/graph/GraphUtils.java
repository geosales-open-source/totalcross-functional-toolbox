package br.com.softsite.toolbox.graph;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import br.com.softsite.toolbox.stream.Stream;

public class GraphUtils {

	private GraphUtils() {}

	public static <NODE> boolean grafoAciclico(NODE pontoInicial, Function<NODE, List<NODE>> pegarVizinhos) {
		if (pontoInicial == null) {
			return true;
		}
		return grafoAciclico(pontoInicial, new HashSet<>(), new HashSet<>(), pegarVizinhos);
	}

	public static <NODE> boolean grafoAciclico(List<NODE> grafo, Function<NODE, List<NODE>> pegarVizinhos) {
		Set<NODE> nodosSeguros = new HashSet<>();
		Set<NODE> nodosInvestigados = new HashSet<>();

		return new Stream<>(grafo).allMatch(nodo -> grafoAciclico(nodo, nodosSeguros, nodosInvestigados, pegarVizinhos));
	}

	private static <NODE> boolean grafoAciclico(NODE pontoPesquisa, Set<NODE> nodosSeguros, Set<NODE> nodosInvestigados, Function<NODE, List<NODE>> pegarVizinhos) {
		if (nodosSeguros.contains(pontoPesquisa)) {
			return true;
		}
		if (nodosInvestigados.contains(pontoPesquisa)) {
			return false;
		}
		List<NODE> vizinhos = pegarVizinhos.apply(pontoPesquisa);
		if (vizinhos.isEmpty()) {
			nodosSeguros.add(pontoPesquisa);
			return true;
		}
		nodosInvestigados.add(pontoPesquisa);
		for (NODE vizinho: vizinhos) {
			if (nodosInvestigados.contains(vizinho)) {
				return false;
			}
			if (!nodosSeguros.contains(vizinho)) {
				if (!grafoAciclico(vizinho, nodosSeguros, nodosInvestigados, pegarVizinhos)) {
					return false;
				}
			}
		}
		nodosInvestigados.remove(pontoPesquisa);
		nodosSeguros.add(pontoPesquisa);
		return true;
	}
}
