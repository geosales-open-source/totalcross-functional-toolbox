package br.com.softsite.toolbox.graph;

import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;

import br.com.softsite.toolbox.HasKey;
import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.collection.MapUtils;

public class MontadorGrafo<V, K> {
	private Function<V, K> getNeighborKey;
	private BiConsumer<V, V> setNeighbor;

	MontadorGrafo() {
	}

	public MontadorGrafo(Function<V, K> getNeighborKey, BiConsumer<V, V> setNeighbor) {
		this.getNeighborKey = getNeighborKey;
		this.setNeighbor = setNeighbor;
	}

	public static <C, N> MontadorGrafo<N, C> getMontadorGrafo(Function<N, C> getNeighborKey, BiConsumer<N, N> setNeighbor) {
		return new MontadorGrafo<>(getNeighborKey, setNeighbor);
	}

	public Map<K, V> montarGrafoAlgoritmo(Map<K, V> keyMap, Iterable<V> itens) {
		for (V item: itens) {
			if (item != null) {
				setNeighbor.accept(item, Toolbox.getIfNotNull(getNeighborKey.apply(item), keyMap::get));
			}
		}

		return keyMap;
	}

	public Map<K, V> montarGrafoAlgoritmo(Iterable<V> itens, Function<V, K> getPrimaryKey) {
		return montarGrafoAlgoritmo(MapUtils.getMapFromKey(itens, getPrimaryKey), itens);
	}

	@SafeVarargs
	public static <C, N extends HasKey<C>> Map<C, N> montarGrafo(Iterable<N> itens, MontadorGrafo<N, C>... montadores) {
		return montarGrafo(itens, MapUtils.getMapFromKey(itens, HasKey::getKey), montadores);
	}

	@SafeVarargs
	public static <C, N> Map<C, N> montarGrafo(Iterable<N> itens, Map<C, N> keyMap, MontadorGrafo<N, C>... montadores) {
		for (MontadorGrafo<N, C> montador: montadores) {
			montador.montarGrafoAlgoritmo(keyMap, itens);
		}

		return keyMap;
	}
}
