package br.com.softsite.toolbox.optional;

import java.util.NoSuchElementException;
import java.util.function.LongConsumer;
import java.util.function.LongSupplier;
import java.util.function.Supplier;

import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.LongStream;

public final class OptionalLong {
	private static OptionalLong empty = new OptionalLong();
	
	private boolean isEmpty;
	private long t;

	private OptionalLong() {
		this.isEmpty = true;
	}
	
	private OptionalLong(long t) {
		this.isEmpty = false;
		this.t = t;
	}
	
	public void orElseThrow() {
		orElseThrow(() -> new NoSuchElementException("Empty optional"));
	}
	
	public <X extends Throwable> void orElseThrow(Supplier<X> exceptionSupplier) throws X {
		if (isEmpty) {
			throw exceptionSupplier.get();
		}
	}
	
	public long getAsLong() {
		if (isEmpty) {
			throw new NoSuchElementException("Empty optional");
		}
		
		return t;
	}
	
	public void ifPresent(LongConsumer action) {
		ifPresentOrElse(action, FunctionUtils.noop());
	}
	
	public void ifPresentOrElse(LongConsumer action, Runnable emptyAction) {
		if (!isEmpty) {
			action.accept(t);
		} else {
			emptyAction.run();
		}
	}
	
	public boolean isEmpty() {
		return isEmpty;
	}
	
	public boolean isPresent() {
		return !isEmpty;
	}
	
	public long orElseGet(LongSupplier orF) {
		return isEmpty? orF.getAsLong(): t;
	}
	
	public long orElse(long orT) {
		return isEmpty? orT: t;
	}
	
	public LongStream stream() {
		return isEmpty? LongStream.empty(): LongStream.of(t);
	}
	
	@Override
	public String toString() {
		return isEmpty? "Empty int optional": "Non-empty int optional: " + t;
	}
	
	@Override
	public int hashCode() {
		return isEmpty? 0: (int) t;
	}
	
	public static OptionalLong of(long t) {
		return new OptionalLong(t);
	}
	
	public static OptionalLong empty() {
		return empty;
	}
}
