package br.com.softsite.toolbox.stream;

interface IntIterable {
	IntIterator iterator();
}
