package br.com.softsite.toolbox.stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.collection.MapUtils;
import br.com.softsite.toolbox.indirection.DoubleIndirection;
import br.com.softsite.toolbox.indirection.Indirection;
import br.com.softsite.toolbox.indirection.IntIndirection;
import br.com.softsite.toolbox.indirection.LongIndirection;
import br.com.softsite.toolbox.math.DoubleSummaryStatistics;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.math.IntSummaryStatistics;
import br.com.softsite.toolbox.math.LongSummaryStatistics;
import br.com.softsite.toolbox.optional.Optional;

public class Collectors {
	public static <T, R> Collector<T, R, R> of(Supplier<R> supplier, BiConsumer<R, T> accumulator, BiFunction<R, R, R> combiner) {
		return of(supplier, accumulator, combiner, FunctionUtils::identity);
	}
	
	public static <T, A, R> Collector<T, A, R> of(Supplier<A> supplier, BiConsumer<A, T> accumulator, BiFunction<A, A, A> combiner, Function<A, R> finisher) {
		return new Collector<T, A, R>() {

			@Override
			public BiConsumer<A, T> accumulator() {
				return accumulator;
			}

			@Override
			public BiFunction<A, A, A> combiner() {
				return combiner;
			}

			@Override
			public Function<A, R> finisher() {
				return finisher;
			}

			@Override
			public Supplier<A> supplier() {
				return supplier;
			}
		};
	}
	
	public static <T> Collector<T, ?, Optional<T>> reducing(BiFunction<T, T, T> reduce) {
		return Collectors.<T, Indirection<Optional<T>>, Optional<T>>of(() -> {
			return new Indirection<>(Optional.empty());
		}, (ind, v) -> {
			ind.x.ifPresentOrElse((o) -> ind.x = Optional.of(reduce.apply(o, v)), () -> ind.x = Optional.of(v));
		}, (ind1, ind2) -> {
			ind2.x.ifPresent((v) -> ind1.x.ifPresentOrElse((o) -> ind1.x = Optional.of(reduce.apply(o, v)), () -> { ind1.x = ind2.x; }));
			return ind1;
		}, Indirection::getX
		);
	}
	
	public static <T> Collector<T, ?, T> reducing(T neutral, BiFunction<T, T, T> reduce) {
		return Collectors.<T, Indirection<T>, T>of(() -> {
			return new Indirection<>(neutral);
		}, (ind, v) -> {
			ind.x = reduce.apply(ind.x, v);
		}, (ind1, ind2) -> {
			ind1.x = reduce.apply(ind1.x, ind2.x);
			return ind1;
		}, Indirection::getX
		);
	}
	
	public static <T, U> Collector<T, ?, U> reducing(U neutral, Function<T, U> map, BiFunction<U, U, U> reduce) {
		return Collectors.<T, Indirection<U>, U>of(() -> {
			return new Indirection<>(neutral);
		}, (ind, v) -> {
			ind.x = reduce.apply(ind.x, map.apply(v));
		}, (ind1, ind2) -> {
			ind1.x = reduce.apply(ind1.x, ind2.x);
			return ind1;
		}, Indirection::getX
		);
	}
	
	public static <T> Collector<T, ?, Double> summingDouble(ToDoubleFunction<T> mapper) {
		return Collectors.<T, DoubleIndirection, Double>of(() -> {
			return new DoubleIndirection(0);
		}, (a, t) -> {
			a.x += mapper.applyAsDouble(t);
		}, (a1, a2) -> {
			a1.x += a2.x;
			return a1;
		}, DoubleIndirection::getX);
	}
	
	public static <T> Collector<T, ?, Integer> summingInt(ToIntFunction<T> mapper) {
		return Collectors.<T, IntIndirection, Integer>of(() -> {
			return new IntIndirection(0);
		}, (a, t) -> {
			a.x += mapper.applyAsInt(t);
		}, (a1, a2) -> {
			a1.x += a2.x;
			return a1;
		}, IntIndirection::getX);
	}
	
	public static <T> Collector<T, ?, Long> summingLong(ToLongFunction<T> mapper) {
		return Collectors.<T, LongIndirection, Long>of(() -> {
			return new LongIndirection(0);
		}, (a, t) -> {
			a.x += mapper.applyAsLong(t);
		}, (a1, a2) -> {
			a1.x += a2.x;
			return a1;
		}, LongIndirection::getX);
	}
	
	public static <T, C extends Collection<T>> Collector<T, ?, C> toCollection(Supplier<C> collectionSupplier) {
		return Collectors.<T, C>of(collectionSupplier, Collection::add, CollectionUtils::addAll);
	}
	
	public static <T> Collector<T, ?, List<T>> toList() {
		return toCollection(ArrayList::new);
	}
	
	public static <T> Collector<T, ?, Set<T>> toSet() {
		return toCollection(HashSet::new);
	}
	
	public static <T, K, V, M extends Map<K, V>> Collector<T, ?, M> toMap(Function<T, K> toKey,
			Function<T, V> toValue, BiFunction<V, V, V> mergeValues, Supplier<M> mapSupplier) {
		return Collectors.<T, M>of(mapSupplier, (m, t) -> {
			MapUtils.accumulate(m, toKey.apply(t), t, toValue, mergeValues);
		}, (m1, m2) -> {
			new Stream<>(m2.entrySet()).forEach(e -> {
				K key = e.getKey();
				m1.put(key, mergeValues.apply(m1.get(key), e.getValue()));
			});
			return m1;
		});
	}
	
	public static <T, K, V> Collector<T, ?, Map<K, V>> toMap(Function<T, K> toKey,
			Function<T, V> toValue, BiFunction<V, V, V> mergeValues) {
		return toMap(toKey, toValue, mergeValues, HashMap::new);
	}
	
	public static <T, K, V> Collector<T, ?, Map<K, V>> toMap(Function<T, K> toKey,
			Function<T, V> toValue) {
		return toMap(toKey, toValue, (v1, v2) -> {
			if (v1 != null) {
				if (v2 != null) {
					throw new IllegalStateException("Conflicting keys");
				}
				return v1;
			}
			return v2;
		});
	}
	
	public static <T, K> Collector<T, ?, Map<K, List<T>>> groupingBy(Function<T, K> classifier) {
		return Collectors.<T, K, List<T>>toMap(classifier, CollectionUtils::toMutableSingleList, CollectionUtils::addAll);
	}
	
	public static <T, K, A, D> Collector<T, ?, Map<K, D>> groupingBy(Function<T, K> classifier, Collector<T, A, D> collector) {
		return groupingBy(classifier, HashMap::new, collector);
	}
	
	public static <T, K, A, D, M extends Map<K, D>> Collector<T, ?, M> groupingBy(Function<T, K> classifier, Supplier<M> mapSupplier, Collector<T, A, D> collector) {
		return Collectors.<T, Map<K, A>, M>of(HashMap::new, (m, t) -> {
			K key = classifier.apply(t);
			A accumulator = m.get(key);
			if (accumulator == null) {
				accumulator = collector.supplier().get();
				m.put(key, accumulator);
			}
			collector.accumulator().accept(accumulator, t);
		}, (m1, m2) -> {
			new Stream<>(m2.entrySet()).forEach(e -> {
				K key = e.getKey();
				m1.put(key, Toolbox.getIfNotNull(m1.get(key), accumulator -> collector.combiner().apply(accumulator, e.getValue()), e.getValue()));
			});
			return m1;
		}, (m) -> {
			return new Stream<>(m.entrySet()).collect(Collectors.toMap(Entry::getKey, FunctionUtils.compose(Entry::getValue, collector.finisher()), (m1, m2) -> {
				return m1; // jamais deve ocorrer, todas as chaves já foram mergeadas antes
			}, mapSupplier));
		});
	}
	
	public static <T, A, R, RR> Collector<T, A, RR> collectingAndThen(Collector<T, A, R> downstream, Function<R, RR> finisher) {
		return Collectors.<T, A, RR>of(downstream.supplier(), downstream.accumulator(), downstream.combiner(), FunctionUtils.compose(downstream.finisher(), finisher));
	}
	
	public static <T,A,R> Collector<T,?,R> filtering(Predicate<T> test, Collector<T, A, R> downstream) {
		BiConsumer<A, T> accumulator = downstream.accumulator();
		return Collectors.<T, A, R>of(downstream.supplier(), (a, t) -> {
			if (test.test(t)) {
				accumulator.accept(a, t);
			}
		}, downstream.combiner(), downstream.finisher());
	}
	
	public static Collector<CharSequence, ?, String> joining() {
		return joining("");
	}

	public static Collector<CharSequence, ?, String> joining(CharSequence delimiter) {
		return joining(delimiter, "", "");
	}

	private static class JoiningAcc {
		boolean first = true;
		StringBuilder sb;

		JoiningAcc(CharSequence c) {
			sb = new StringBuilder(c.toString());
		}
	}

	public static Collector<CharSequence, ?, String> joining(CharSequence delimiter, CharSequence prefix, CharSequence suffix) {
		return Collectors.<CharSequence, JoiningAcc, String>of(() -> new JoiningAcc(prefix), (ac, s) -> {
			StringBuilder sb = ac.sb;
			if (ac.first) {
				ac.first = false;
			} else {
				sb.append(delimiter);
			}
			sb.append(s);
		}, (ac1, ac2) -> {
			if (ac1.first) {
				return ac2;
			} else if (ac2.first) {
				return ac1;
			}
			ac1.sb.append(delimiter).append(ac2.sb);
			return ac1;
		}, ac -> ac.sb.append(suffix).toString());
	}
	
	public static <T> Collector<T, ?, Long> counting() {
		return summingLong(x -> 1);
	}
	
	public static <T> Collector<T, ?, IntSummaryStatistics> summarizingInt(ToIntFunction<T> f) {
		return Collectors.<T, IntSummaryStatistics>of(IntSummaryStatistics::new, (st, t) -> st.accept(f.applyAsInt(t)), (a, b) -> { a.combine(b); return a; });
	}
	
	public static <T> Collector<T, ?, LongSummaryStatistics> summarizingLong(ToLongFunction<T> f) {
		return Collectors.<T, LongSummaryStatistics>of(LongSummaryStatistics::new, (st, t) -> st.accept(f.applyAsLong(t)), (a, b) -> { a.combine(b); return a; });
	}
	
	public static <T> Collector<T, ?, DoubleSummaryStatistics> summarizingDouble(ToDoubleFunction<T> f) {
		return Collectors.<T, DoubleSummaryStatistics>of(DoubleSummaryStatistics::new, (st, t) -> st.accept(f.applyAsDouble(t)), (a, b) -> { a.combine(b); return a; });
	}
	
	public static <T> Collector<T, ?, Double> averagingInt(ToIntFunction<T> f) {
		return collectingAndThen(summarizingInt(f), IntSummaryStatistics::getAverage);
	}
	
	public static <T> Collector<T, ?, Double> averagingLong(ToLongFunction<T> f) {
		return collectingAndThen(summarizingLong(f), LongSummaryStatistics::getAverage);
	}
	
	public static <T> Collector<T, ?, Double> averagingDouble(ToDoubleFunction<T> f) {
		return collectingAndThen(summarizingDouble(f), DoubleSummaryStatistics::getAverage);
	}
	
	public static <T, U, A, R> Collector<T, ?, R> mapping(Function<T, U> f, Collector<U, A, R> collector) {
		return Collectors.<T, A, R>of(collector.supplier(), (a, t) -> collector.accumulator().accept(a, f.apply(t)), collector.combiner(), collector.finisher());
	}
	
	public static <T> Collector<T, ?, Optional<T>> maxBy(Comparator<T> comparator) {
		return reducing((a, b) -> comparator.compare(a, b) > 0? a: b);
	}
	
	public static <T> Collector<T, ?, Optional<T>> minBy(Comparator<T> comparator) {
		return reducing((a, b) -> comparator.compare(a, b) < 0? a: b);
	}
	
	public static <T> Collector<T, ?, Map<Boolean, List<T>>> partitioningBy(Predicate<T> condicao) {
		return Collectors.<T, Pair<ArrayList<T>, ArrayList<T>>, Map<Boolean, List<T>>>of(
				() -> Pair.getPair(new ArrayList<>(), new ArrayList<>()),
				(p, t) -> {
					ArrayList<T> l;
					if (condicao.test(t)) {
						l = p.getKey();
					} else {
						l = p.getValue();
					}
					l.add(t);
				},
				(p1, p2) -> {
					p1.getKey().addAll(p2.getKey());
					p1.getValue().addAll(p2.getValue());
					return p1;
				},
				p -> {
					HashMap<Boolean, List<T>> hm = new HashMap<>();
					hm.put(true, p.getKey());
					hm.put(false, p.getValue());
					return hm;
				});
	}
	
	public static <T, D, A> Collector<T, ?, Map<Boolean, D>> partitioningBy(Predicate<T> condicao, Collector<T, A, D> coletor) {
		return collectingAndThen(partitioningBy(condicao), hm -> {
			HashMap<Boolean, D> hm2 = new HashMap<>();
			hm2.put(true, new Stream<>(hm.get(true)).collect(coletor));
			hm2.put(false, new Stream<>(hm.get(false)).collect(coletor));
			return hm2;
		});
	}
	
	public static <T, U, D, A> Collector<T, ?, D> flatMapping(Function<T, Stream<U>> flatMapper, Collector<U, A, D> coletor) {
		BiConsumer<A, U> accumulator = coletor.accumulator();
		return Collectors.<T, A, D>of(coletor.supplier(),
				(c, t) -> flatMapper.apply(t).forEach(u -> accumulator.accept(c, u)),
				coletor.combiner(),
				coletor.finisher());
	}
}
