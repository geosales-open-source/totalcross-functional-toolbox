package br.com.softsite.toolbox.indirection;

public class Indirection<X> {
	public X x;
	
	public Indirection() {
	}
	
	public Indirection(X x) {
		this.x = x;
	}
	
	public X getX() {
		return x;
	}
}
