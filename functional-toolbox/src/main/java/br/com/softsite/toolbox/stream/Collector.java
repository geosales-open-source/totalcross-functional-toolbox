package br.com.softsite.toolbox.stream;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public interface Collector<T, A, R> {
	BiConsumer<A, T> accumulator();
	BiFunction<A, A, A> combiner();
	Function<A, R> finisher();
	Supplier<A> supplier();
}
