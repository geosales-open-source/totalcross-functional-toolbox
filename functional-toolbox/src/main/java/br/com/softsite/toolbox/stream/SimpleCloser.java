package br.com.softsite.toolbox.stream;

import java.util.ArrayList;

import br.com.softsite.toolbox.math.FunctionUtils;

class SimpleCloser {

	static final SimpleCloser EMPTY = new SimpleCloser(null, null) {

		@Override
		public void close() {
		}
	};

	static Runnable compose(SimpleCloser closer1, SimpleCloser closer2) {
		boolean empty1 = closer1 == EMPTY;
		boolean empty2 = closer2 == EMPTY;
		if (empty1 && empty2) {
			return FunctionUtils.noop();
		} else if (empty1) {
			return closer2::close;
		} else if (empty2) {
			return closer1::close;
		}
		return new SimpleCloser(closer1::close, closer2)::close;
	}

	static void closeAll(ArrayList<SimpleCloser> alsoClose) {
		if (alsoClose.isEmpty()) {
			return;
		}
		SimpleCloser closer = EMPTY;
		for (SimpleCloser closerElement: alsoClose) {
			if (closerElement == EMPTY) {
				continue;
			}
			closer = new SimpleCloser(closerElement::close, closer);
		}
		closer.close();
	}

	private final SimpleCloser next;
	private final Runnable value;

	SimpleCloser(Runnable value) {
		this(value, EMPTY);
	}

	SimpleCloser(Runnable value, SimpleCloser next) {
		this.value = value;
		this.next = next;
	}

	void close() {
		try {
			value.run();
		} catch (Throwable t1) {
			try {
				next.close();
			} catch (Throwable t2) {
				t1.addSuppressed(t2);
			}
			throw t1;
		}
		next.close();
	}
}
