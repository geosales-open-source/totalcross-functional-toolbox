package br.com.softsite.toolbox.collection;

import java.util.Iterator;

class ImmutableIterator<T> implements Iterator<T> {

	private final Iterator<T> underlying;

	ImmutableIterator(Iterator<T> underlying) {
		this.underlying = underlying;
	}

	@Override
	public boolean hasNext() {
		return underlying.hasNext();
	}

	@Override
	public T next() {
		return underlying.next();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("ImmutableIterator cannot alter its elements");
	}
}
