package br.com.softsite.toolbox;

import java.util.Collection;
import java.util.function.Function;

import br.com.softsite.toolbox.math.FunctionUtils;

public class PairBuilder<K, V> implements IsSerializable {
	
	public static <K,V> Function<K, V> toInjectiveFunction(Collection<PairBuilder<K,V>> mapping) {
		return FunctionUtils.toInjectiveFunction(mapping, PairBuilder::getKey, PairBuilder::getValue);
	}
	
	private K key;
	private V value;
	
	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public PairBuilder() {
	}
	
	public PairBuilder(K key, V value) {
		this.key = key;
		this.value = value;
	}
	
	public Pair<K,V> generatePair() {
		return new Pair<K, V>(key, value);
	}
	
	@Override
	public int hashCode() {
		return (key != null? key.hashCode(): 0) ^ (value != null? value.hashCode(): 0);
	}
	
	@Override
	public boolean equals(Object other) {
	    if (other == null) {
	    	return false;
	    } else if (other == this) {
	    	return true;
	    } else if (!(other instanceof PairBuilder)) {
	    	return false;
	    }
	    PairBuilder<?,?> otherMyClass = (PairBuilder<?,?>)other;
	    
	    return ((key == otherMyClass.getKey()) || (key != null && key.equals(otherMyClass.getKey()))) &&
	    		((value == otherMyClass.getValue()) || (value != null && value.equals(otherMyClass.getValue())));
	}
}
