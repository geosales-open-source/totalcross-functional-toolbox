package br.com.softsite.toolbox.stream;

public class StreamHolder<S extends BaseStream<?, S>> implements AutoCloseable {

	public final S baseStream;
	public StreamHolder(S baseStream) {
		this.baseStream = baseStream;
	}

	@Override
	public void close() {
		baseStream.close();
	}
}
