package br.com.softsite.toolbox.stream;

import java.util.Iterator;
import java.util.NoSuchElementException;

class IntIterableUtils {

	private IntIterableUtils() {
	}

	static IntIterable toIntIterable(Iterable<Integer> iterable) {
		return () -> new IntIterator() {
			Iterator<Integer> iterator = iterable.iterator();
			
			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}
			
			@Override
			public int next() {
				return iterator.next();
			}
		};
	}

	static Iterable<Integer> toIterable(IntIterable iterable) {
		return () -> new Iterator<Integer>() {
			IntIterator iterator = iterable.iterator();
			
			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}
			
			@Override
			public Integer next() {
				return iterator.next();
			}
		};
	}

	static IntIterable toIntIterable(int[] a) {
		return () -> new IntIterator() {
			int idx = 0;
			
			@Override
			public boolean hasNext() {
				return idx < a.length;
			}
			
			@Override
			public int next() {
				if (idx >= a.length) {
					throw new NoSuchElementException("End of iteration");
				}
				int e = a[idx];
				idx++;
				return e;
			}
		};
	}

	static IntIterable toIntIterable(int a) {
		return () -> new IntIterator() {
			boolean hasNext = true;
			
			@Override
			public boolean hasNext() {
				return hasNext;
			}
			
			@Override
			public int next() {
				if (!hasNext) {
					throw new NoSuchElementException("End of iteration");
				}
				hasNext = false;
				return a;
			}
		};
	}

}
