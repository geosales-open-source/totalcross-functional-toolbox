package br.com.softsite.toolbox.optional;

import java.util.NoSuchElementException;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.IntStream;

public final class OptionalInt {
	private static OptionalInt empty = new OptionalInt();
	
	private boolean isEmpty;
	private int t;

	private OptionalInt() {
		this.isEmpty = true;
	}
	
	private OptionalInt(int t) {
		this.isEmpty = false;
		this.t = t;
	}
	
	public void orElseThrow() {
		orElseThrow(() -> new NoSuchElementException("Empty optional"));
	}
	
	public <X extends Throwable> void orElseThrow(Supplier<X> exceptionSupplier) throws X {
		if (isEmpty) {
			throw exceptionSupplier.get();
		}
	}
	
	public int getAsInt() {
		if (isEmpty) {
			throw new NoSuchElementException("Empty optional");
		}
		
		return t;
	}
	
	public void ifPresent(IntConsumer action) {
		ifPresentOrElse(action, FunctionUtils.noop());
	}
	
	public void ifPresentOrElse(IntConsumer action, Runnable emptyAction) {
		if (!isEmpty) {
			action.accept(t);
		} else {
			emptyAction.run();
		}
	}
	
	public boolean isEmpty() {
		return isEmpty;
	}
	
	public boolean isPresent() {
		return !isEmpty;
	}
	
	public int orElseGet(IntSupplier orF) {
		return isEmpty? orF.getAsInt(): t;
	}
	
	public int orElse(int orT) {
		return isEmpty? orT: t;
	}
	
	public IntStream stream() {
		return isEmpty? IntStream.empty(): IntStream.of(t);
	}
	
	@Override
	public String toString() {
		return isEmpty? "Empty int optional": "Non-empty int optional: " + t;
	}
	
	@Override
	public int hashCode() {
		return isEmpty? 0: t;
	}
	
	public static OptionalInt of(int t) {
		return new OptionalInt(t);
	}
	
	public static OptionalInt empty() {
		return empty;
	}
}
