package br.com.softsite.toolbox.stream;

import java.util.Iterator;

public interface BaseStream<T, S extends BaseStream<T, S>> {

	Iterator<T> iterator();
	S onClose(Runnable r);
	void close();
}
