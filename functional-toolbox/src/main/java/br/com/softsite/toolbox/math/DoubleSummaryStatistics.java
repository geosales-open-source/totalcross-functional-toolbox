package br.com.softsite.toolbox.math;

import java.util.function.DoubleConsumer;

public class DoubleSummaryStatistics implements DoubleConsumer {
	private double min = Double.POSITIVE_INFINITY;
	private double max = Double.NEGATIVE_INFINITY;
	private long count = 0;
	private double sum = 0;
	private boolean dirty = false;
	private double average = 0;
	
	public void combine(DoubleSummaryStatistics another) {
		if (another.count != 0) {
			dirty = true;
			
			count += another.count;
			sum += another.sum;
			if (another.min < min) {
				min = another.min;
			}
			if (another.max > max) {
				max = another.max;
			}
			
		}
	}
	
	@Override
	public void accept(double value) {
		if (count != 0) {
			count++;
			sum += value;
			
			if (value < min) {
				min = value;
			} else if (value > max) {
				max = value;
			}
		} else {
			count = 1;
			sum = min = max = value;
		}
		dirty = true;
	}
	
	public double getMax() {
		return max;
	}
	
	public double getMin() {
		return min;
	}
	
	public long getCount() {
		return count;
	}
	
	public double getSum() {
		return sum;
	}
	
	public double getAverage() {
		if (dirty) {
			if (count == 0) {
				average = 0;
			} else {
				average = sum/((double) count);
			}
			dirty = false;
		}
		return average;
	}
}
