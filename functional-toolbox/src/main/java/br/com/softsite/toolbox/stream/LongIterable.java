package br.com.softsite.toolbox.stream;

interface LongIterable {
	LongIterator iterator();
}
