package br.com.softsite.toolbox;

public interface HasKey<K> {
	K getKey();
}
