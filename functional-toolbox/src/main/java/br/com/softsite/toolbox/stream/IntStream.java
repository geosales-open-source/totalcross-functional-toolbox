package br.com.softsite.toolbox.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiConsumer;
import java.util.function.IntBinaryOperator;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntToLongFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.ObjIntConsumer;
import java.util.function.Supplier;

import br.com.softsite.toolbox.Toolbox;
import br.com.softsite.toolbox.indirection.Indirection;
import br.com.softsite.toolbox.indirection.IntIndirection;
import br.com.softsite.toolbox.indirection.LongIndirection;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.math.IntSummaryStatistics;
import br.com.softsite.toolbox.optional.OptionalDouble;
import br.com.softsite.toolbox.optional.OptionalInt;

public class IntStream implements BaseStream<Integer, IntStream> {
	IntIterable iterable;
	SimpleCloser closes;
	public IntStream(IntIterable iterable) {
		this(SimpleCloser.EMPTY, iterable);
	}

	IntStream(SimpleCloser closes, IntIterable iterable) {
		this.iterable = iterable;
		this.closes = closes;
	}

	// intermediate operations

	@Override
	public IntStream onClose(Runnable closeHandler) {
		closes = new SimpleCloser(closeHandler, closes);
		return this;
	}

	public IntStream filter(IntPredicate filter) {
		return new IntStream(closes, () -> new IntIterator() {
			IntIterator iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			int valorFetch;

			void fetchNext() {
				firstRun = false;

				while (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (filter.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public int next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				int valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public <K> Stream<K> mapToObj(IntFunction<K> map) {
		return new Stream<>(closes, () -> new Iterator<K>() {
			IntIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public K next() {
				return map.apply(iterator.next());
			}
		});
	}

	public DoubleStream mapToDouble(IntToDoubleFunction map) {
		return new DoubleStream(closes, () -> new DoubleIterator() {
			IntIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public double next() {
				return map.applyAsDouble(iterator.next());
			}
		});
	}

	public LongStream mapToLong(IntToLongFunction map) {
		return new LongStream(closes, () -> new LongIterator() {
			IntIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public long next() {
				return map.applyAsLong(iterator.next());
			}
		});
	}

	public IntStream map(IntUnaryOperator map) {
		return new IntStream(closes, () -> new IntIterator() {
			IntIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public int next() {
				return map.applyAsInt(iterator.next());
			}
		});
	}

	public IntStream flatMap(IntFunction<IntStream> map) {
		ArrayList<SimpleCloser> alsoClose = new ArrayList<>();
		return new IntStream(closes, () -> new IntIterator() {
			IntIterator iteratorStream = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			int valorFetch;
			IntIterator iterator;

			void fetchNext() {
				firstRun = false;

				boolean repeat = false;
				do {
					repeat = false;
					if (iterator != null && iterator.hasNext()) {
						conseguiuFetch = true;
						valorFetch = iterator.next();
						return;
					} else if (iteratorStream.hasNext()) {
						IntStream stream = map.apply(iteratorStream.next());
						if (stream.closes != SimpleCloser.EMPTY) {
							alsoClose.add(stream.closes);
						}
						iterator = stream.iterable.iterator();
						repeat = true;
					}
				} while (repeat);
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public int next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				int valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		}) {

			@Override
			public void close() {
				try {
					super.close();
				} finally {
					SimpleCloser.closeAll(alsoClose);
				}
			}
		};
	}

	public IntStream distinct() {
		// não tem a otimização possível de se fazer se for uma stream ordenada
		HashSet<Integer> x = new HashSet<>();
		return filter(x::add);
	}

	public IntStream sorted() {
		ArrayList<Integer> l = boxed().collect(Collectors.toCollection(ArrayList::new));
		Collections.sort(l, Integer::compareTo);
		return new IntStream(closes, IntIterableUtils.toIntIterable(l));
	}

	public IntStream peek(IntConsumer peeker) {
		return new IntStream(closes, () -> new IntIterator() {
			IntIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				return iterator.hasNext();
			}

			@Override
			public int next() {
				int element = iterator.next();
				peeker.accept(element);
				return element;
			}
		});
	}

	public IntStream limit(long max) {
		return new IntStream(closes, () -> new IntIterator() {
			long x = 0;
			IntIterator iterator = iterable.iterator();

			@Override
			public boolean hasNext() {
				if (x < max) {
					x++;
					return iterator.hasNext();
				} else {
					return false;
				}
			}

			@Override
			public int next() {
				return iterator.next();
			}
		});
	}

	public IntStream skip(long n) {
		LongIndirection limit = new LongIndirection(0);
		return filter(t -> {
			if (limit.x < n) {
				limit.x++;
				return false;
			}
			return true;
		});
	}

	public IntStream takeWhile(IntPredicate test) {
		return new IntStream(closes, () -> new IntIterator() {
			IntIterator iterator = iterable.iterator();
			boolean conseguiuFetch;
			boolean firstRun = true;
			int valorFetch;

			void fetchNext() {
				firstRun = false;

				if (iterator.hasNext()) {
					valorFetch = iterator.next();
					if (test.test(valorFetch)) {
						conseguiuFetch = true;
						return;
					} else {
						conseguiuFetch = false;
						return;
					}
				}
				conseguiuFetch = false;
			}

			@Override
			public boolean hasNext() {
				if (firstRun) {
					fetchNext();
				}
				return conseguiuFetch;
			}

			@Override
			public int next() {
				if (firstRun) {
					fetchNext();
				}
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				int valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public IntStream dropWhile(IntPredicate test) {
		return takeWhile(FunctionUtils.negateIntPredicate(test));
	}

	// terminal operations

	public Stream<Integer> boxed() {
		return new Stream<>(closes, getIterable());
	}

	public void forEach(IntConsumer consumer) {
		for (int element: getIterable()) {
			consumer.accept(element);
		}
	}

	public int[] toArray() {
		int []array = new int[(int) count()];
		IntIndirection i = new IntIndirection(0);

		forEach(t -> {
			array[i.x] = t;
			i.x++;
		});

		return array;
	}

	public int reduce(int neutralElement, IntBinaryOperator accumulator) {
		IntIndirection acc = new IntIndirection(neutralElement);

		forEach(t -> {
			acc.x = accumulator.applyAsInt(acc.x, t);
		});

		return acc.x;
	}

	public OptionalInt reduce(IntBinaryOperator accumulator) {
		Indirection<OptionalInt> x = new Indirection<>(OptionalInt.empty());

		forEach(t -> x.x.ifPresentOrElse(t2 -> x.x = OptionalInt.of(accumulator.applyAsInt(t2, t)), () -> x.x = OptionalInt.of(t)));

		return x.x;
	}

	public long count() {
		LongIndirection i = new LongIndirection(0L);

		forEach(t -> {
			i.x++;
		});

		return i.x;
	}

	public <R> R collect(Supplier<R> supplier, ObjIntConsumer<R> accumulator, BiConsumer<R, R> combiner) {
		R result = supplier.get();

		for (int t: getIterable()) {
			accumulator.accept(result, t);
		}

		return result;
	}

	public OptionalInt max() {
		Integer max = null;

		for (int x: getIterable()) {
			if (max == null) {
				max = x;
			} else if (x > max) {
				max = x;
			}
		}
		return Toolbox.getIfNotNullLazyDefault(max, OptionalInt::of, OptionalInt::empty);
	}

	public OptionalInt min() {
		Integer min = null;

		for (int x: getIterable()) {
			if (min == null) {
				min = x;
			} else if (x < min) {
				min = x;
			}
		}
		return Toolbox.getIfNotNullLazyDefault(min, OptionalInt::of, OptionalInt::empty);
	}

	public boolean anyMatch(IntPredicate matcher) {
		for (int t: getIterable()) {
			if (matcher.test(t)) {
				return true;
			}
		}
		return false;
	}

	public boolean allMatch(IntPredicate matcher) {
		for (int t: getIterable()) {
			if (!matcher.test(t)) {
				return false;
			}
		}
		return true;
	}

	public boolean noneMatch(IntPredicate matcher) {
		return !anyMatch(matcher);
	}

	public OptionalInt findFirst() {
		for (int t: getIterable()) {
			return OptionalInt.of(t);
		}
		return OptionalInt.empty();
	}

	public OptionalInt findAny() {
		return findFirst();
	}

	public DoubleStream asDoubleStream() {
		return this.mapToDouble(i -> (double) i);
	}

	public LongStream asLongStream() {
		return this.mapToLong(i -> (long) i);
	}

	public OptionalDouble average() {
		IntSummaryStatistics statistics = summaryStatistics();
		return statistics.getCount() == 0? OptionalDouble.empty(): OptionalDouble.of(statistics.getAverage());
	}

	public int sum() {
		int s = 0;
		for (int l: getIterable()) {
			s += l;
		}
		return s;
	}

	public IntSummaryStatistics summaryStatistics() {
		return collect(IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine);
	}

	@Override
	public Iterator<Integer> iterator() {
		return getIterable().iterator();
	}

	@Override
	public void close() {
		closes.close();
	}

	// static methods et al

	public static IntStream iterate(int seed, IntPredicate hasNext, IntUnaryOperator next) {
		return new IntStream(() -> new IntIterator() {
			boolean conseguiuFetch = true;
			int valorFetch = seed;

			void fetchNext() {
				conseguiuFetch = hasNext.test(valorFetch);
				valorFetch = next.applyAsInt(valorFetch);
			}

			@Override
			public boolean hasNext() {
				return conseguiuFetch;
			}

			@Override
			public int next() {
				if (!conseguiuFetch) {
					throw new NoSuchElementException("fim da leitura");
				}
				int valorRetorno = valorFetch;
				fetchNext();
				return valorRetorno;
			}
		});
	}

	public static IntStream iterate(int seed, IntUnaryOperator next) {
		return iterate(seed, x -> true, next);
	}

	public static IntStream generate(IntSupplier sup) {
		return iterate(sup.getAsInt(), t -> sup.getAsInt());
	}

	private static final IntStream emptyStream = new IntStream(() -> new IntIterator() {
		@Override
		public boolean hasNext() {
			return false;
		}

		@Override
		public int next() {
			throw new NoSuchElementException("Empty stream");
		}
	}) {
		@Override
		public IntStream onClose(Runnable closeHandler) {
			return new IntStream(new SimpleCloser(closeHandler), this.iterable);
		}
	};

	public static IntStream empty() {
		return emptyStream;
	}

	public static IntStream of(int t) {
		return new IntStream(IntIterableUtils.toIntIterable(t));
	}

	public static IntStream of(int... t) {
		return new IntStream(IntIterableUtils.toIntIterable(t));
	}

	public static IntStream range(int ini, int endExclusive) {
		return new IntStream(() -> new IntIterator() {
			int fetched = ini;
			@Override
			public int next() {
				if (!hasNext()) {
					throw new NoSuchElementException("End of range");
				}
				int atual = fetched;
				fetched++;
				return atual;
			}

			@Override
			public boolean hasNext() {
				return fetched < endExclusive;
			}
		});
	}

	public static IntStream rangeClosed(int ini, int endInclusive) {
		return range(ini, endInclusive + 1);
	}

	public static IntStream concat(IntStream stream1, IntStream stream2) {
		return Stream.of(stream1.boxed(), stream2.boxed()).<Integer>flatMap(FunctionUtils::identity).mapToInt(Integer::intValue).onClose(SimpleCloser.compose(stream1.closes, stream2.closes));
	}

	public static Builder builder() {
		return new Builder() {
			boolean builtState = false;
			ArrayList<Integer> array = new ArrayList<>();

			private void ensureCanGo() {
				if (builtState) {
					throw new IllegalStateException("builder already in terminal state");
				}
			}

			@Override
			public void accept(int t) {
				ensureCanGo();
				array.add(t);
			}

			@Override
			public Builder add(int t) {
				accept(t);
				return this;
			}

			@Override
			public IntStream build() {
				ensureCanGo();
				builtState = true;
				return new IntStream(IntIterableUtils.toIntIterable(array));
			}
		};
	}

	public static interface Builder extends IntConsumer {
		Builder add(int t);
		IntStream build();
	}

	// private aux methods

	private Iterable<Integer> getIterable() {
		return IntIterableUtils.toIterable(iterable);
	}
}
