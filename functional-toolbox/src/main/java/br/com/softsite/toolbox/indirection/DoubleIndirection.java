package br.com.softsite.toolbox.indirection;

public class DoubleIndirection {
	public double x;
	
	public DoubleIndirection() {
	}
	
	public DoubleIndirection(double x) {
		this.x = x;
	}
	
	public double getX() {
		return x;
	}
}
