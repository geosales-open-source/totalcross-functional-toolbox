package br.com.softsite.toolbox.collection;

import java.util.ListIterator;

class ImmutableListIterator<T> implements ListIterator<T> {

	private final ListIterator<T> underlying;

	ImmutableListIterator(ListIterator<T> underlying) {
		this.underlying = underlying;
	}

	@Override
	public boolean hasNext() {
		return underlying.hasNext();
	}

	@Override
	public T next() {
		return underlying.next();
	}

	@Override
	public boolean hasPrevious() {
		return underlying.hasPrevious();
	}

	@Override
	public T previous() {
		return underlying.previous();
	}

	@Override
	public int nextIndex() {
		return underlying.nextIndex();
	}

	@Override
	public int previousIndex() {
		return underlying.previousIndex();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("ImmutableListIterator cannot alter its elements");
	}

	@Override
	public void set(T e) {
		throw new UnsupportedOperationException("ImmutableListIterator cannot alter its elements");
	}

	@Override
	public void add(T e) {
		throw new UnsupportedOperationException("ImmutableListIterator cannot alter its elements");
	}
}
