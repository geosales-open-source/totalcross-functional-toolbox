package br.com.softsite.toolbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiFunction;

import org.junit.Assert;
import org.junit.Test;

import br.com.softsite.toolbox.collection.MapUtils;

public class MapUtilsTest {
	@Test
	public void merge__stable() {
		HashMap<Integer, String> m = new HashMap<>();
		MapUtils.merge(m, 1, "abc", String::concat);
		MapUtils.merge(m, 1, "def", String::concat);

		assertEquals("abcdef", m.get(1));
	}

	@Test
	public void computeIfAbsent__absent1() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertEquals(1, (int) MapUtils.computeIfAbsent(m, 1, __ -> 1));
		assertEquals(1, (int) m.get(1));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfAbsent__absent2() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertEquals(2, (int) MapUtils.computeIfAbsent(m, 1, __ -> 2));
		assertEquals(2, (int) m.get(1));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfAbsent__absentInc() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertEquals(6, (int) MapUtils.computeIfAbsent(m, 5, i -> i + 1));
		assertEquals(6, (int) m.get(5));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfAbsent__present1() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(1, 100);
		assertEquals(100, (int) MapUtils.computeIfAbsent(m, 1, __ -> 1));
		assertEquals(100, (int) m.get(1));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfAbsent__present2() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(1, 100);
		assertEquals(100, (int) MapUtils.computeIfAbsent(m, 1, __ -> 2));
		assertEquals(100, (int) m.get(1));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfAbsent__presentInc() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(5, 100);
		assertEquals(100, (int) MapUtils.computeIfAbsent(m, 5, i -> i + 1));
		assertEquals(100, (int) m.get(5));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfPresent__absent1() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertNull(MapUtils.computeIfPresent(m, 1, (k, v) -> 1));
		assertFalse(m.containsKey(1));
		assertEquals(0, m.size());
	}

	@Test
	public void computeIfPresent__absent2() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertNull(MapUtils.computeIfPresent(m, 1, (k, v) -> 1));
		assertEquals(0, m.size());
	}

	@Test
	public void computeIfPresent__absentSum() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertNull(MapUtils.computeIfPresent(m, 5, (k, v) -> k + v));
		assertEquals(0, m.size());
	}

	@Test
	public void computeIfPresent__present1() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(1, 100);
		assertEquals(1, (int) MapUtils.computeIfPresent(m, 1, (k, v) -> 1));
		assertEquals(1, (int) m.get(1));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfPresent__present2() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(1, 100);
		assertEquals(2, (int) MapUtils.computeIfPresent(m, 1, (k, v) -> 2));
		assertEquals(2, (int) m.get(1));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfPresent__presentSum() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(5, 100);
		assertEquals(105, (int) MapUtils.computeIfPresent(m, 5, (k, v) -> k + v));
		assertEquals(105, (int) m.get(5));
		assertEquals(1, m.size());
	}

	@Test
	public void computeIfPresent__presentRemoving() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(5, 100);
		assertNull(MapUtils.computeIfPresent(m, 5, (k, v) -> null));
		assertEquals(0, m.size());
	}
	@Test
	public void getOrDefault__presentNullValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertEquals(10, (int) MapUtils.getOrDefault(m, 0, 10));
	}
	@Test
	public void getOrDefault__presentValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(0, 20);
		assertEquals(20, (int) MapUtils.getOrDefault(m, 0, 10));
	}
	@Test
	public void putIfAbsent__presentValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(0, 10);
		assertNotNull(MapUtils.putIfAbsent(m, 0, 10));
	}
	@Test
	public void putIfAbsent__presentNullValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertNull(MapUtils.putIfAbsent(m, 0, 10));
	}
	@Test
	public void remove__presentValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(0, 10);
		assertTrue(MapUtils.remove(m, 0, 10));
	}
	@Test
	public void remove__presentNullValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertFalse(MapUtils.remove(m, 0, 10));
	}

	@Test
	public void replace__presentKey() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(0, 10);
		assertNotNull(MapUtils.replace(m, 0, 10));
	}
	@Test
	public void replace__presentNullKey() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(0, 10);
		assertNotNull(MapUtils.replace(m, 0, 10));
	}

	@Test
	public void replace__presentValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		m.put(0, 10);
		assertTrue(MapUtils.replace(m, 0, 10, 20));
	}
	@Test
	public void replace__presentNoValue() {
		HashMap<Integer, Integer> m = new HashMap<>();
		assertFalse(MapUtils.replace(m, 0, 10, 20));
	}
	@Test
	public void replaceAll() {
		HashMap<Integer, Integer> m = new HashMap<>();
		HashMap<Integer, Integer> m_t = new HashMap<>();
		m_t.put(0, 1);
		m_t.put(1, 1);
		m_t.put(2, 1);
		m_t.put(3, 1);

		m.put(0, 10);
		m.put(1, 20);
		m.put(2, 30);
		m.put(3, 40);

		MapUtils.replaceAll(m, (key, value) -> 1);

		assertEquals(m, m_t);
	}
}
