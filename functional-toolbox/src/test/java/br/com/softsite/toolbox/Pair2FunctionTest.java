package br.com.softsite.toolbox;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.junit.Test;

import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;

public class Pair2FunctionTest {

	@Test
	public void test() {
		List<Pair<Integer, Integer>> casosQuadraticos =
				Stream.of(
						Pair.getPair(0, 0),
						Pair.getPair(1, 1),
						Pair.getPair(-1, 1),
						Pair.getPair(-2, 4),
						Pair.getPair(2, 4)
				).collect(Collectors.toList());
		Function<Integer, Integer> funcaoQuadratica = Pair.toInjectiveFunction(casosQuadraticos);
		
		for (int i = -2; i <= 2; i++) {
			assertThat("o quadrado de " + i + " deve ser " + (i*i), funcaoQuadratica.apply(i), equalTo(i*i));
		}
		
		List<Pair<Integer, Integer>> casosQuadraticosInversos =
				new Stream<>(casosQuadraticos).map(p -> Pair.getPair(p.getValue(), p.getKey())).collect(Collectors.toList());
		
		Function<Integer, List<Integer>> funcaoQuadraticaInversa = FunctionUtils.toFunction(casosQuadraticosInversos, Pair::getKey, Pair::getValue);
		assertThat("a raiz quadrada de 0 deve ser {0}", funcaoQuadraticaInversa.apply(0).size(), equalTo(1));
		assertThat("a raiz quadrada de 0 deve ser {0}", funcaoQuadraticaInversa.apply(0).get(0), equalTo(0));
		for (int i = 1; i <= 2; i++) {
			assertThat("a raiz quadrada de " + (i*i) + " deve ser {" + i + "," + (-i) + "}", funcaoQuadraticaInversa.apply(i*i).size(), equalTo(2));
			assertThat("a raiz quadrada de " + (i*i) + " deve ser {" + i + "," + (-i) + "}", Math.abs(funcaoQuadraticaInversa.apply(i*i).get(0)), equalTo(i));
			assertThat("a raiz quadrada de " + (i*i) + " deve ser {" + i + "," + (-i) + "}", Math.abs(funcaoQuadraticaInversa.apply(i*i).get(1)), equalTo(i));
			assertThat("a raiz quadrada de " + (i*i) + " deve ser {" + i + "," + (-i) + "}", funcaoQuadraticaInversa.apply(i*i).get(0), not(equalTo(funcaoQuadraticaInversa.apply(i*i).get(1))));
		}
	}
	
	@Test
	public void testVazio() {
		List<Pair<String, String>> vazio = new ArrayList<>();
		Function<String, List<String>> fVazia = FunctionUtils.toFunction(vazio, Pair::getKey, Pair::getValue);

		assertThat("Função vazia deve retornar sempre nulo", fVazia.apply("marmota").isEmpty());
	}
	
	@Test
	public void testMissMatch() {
		List<Pair<String, String>> listaDummy = Arrays.asList(Pair.getPair("abc", "ABC"), Pair.getPair("abc", "abc"));
		Function<String, List<String>> fDummy = FunctionUtils.toFunction(listaDummy, Pair::getKey, Pair::getValue);

		assertThat("Função não deve pegar corretamente valor distinto de 'abc'", fDummy.apply("marmota").isEmpty());
		assertThat("Função em 'abc' tem dois resultados", fDummy.apply("abc").size(), equalTo(2));
	}
}
