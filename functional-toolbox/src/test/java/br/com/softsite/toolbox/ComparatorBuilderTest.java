package br.com.softsite.toolbox;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import org.junit.Test;

import br.com.softsite.toolbox.indirection.Indirection;
import br.com.softsite.toolbox.math.ComparatorBuilder;
import br.com.softsite.toolbox.math.ComparatorUtils;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;

public class ComparatorBuilderTest {
	static final String testes[] = { "abc", "abb", "aaa", "def", "daa"};

	@Test
	public void test() {
		
		Function<String, Character> firstChar = s -> s.charAt(0);
		Function<String, Character> secondChar = s -> s.charAt(1);
		Function<String, Character> thirdChar = s -> s.charAt(2);
		
		Comparator<String> cmpFirstViaBuilder = ComparatorBuilder.generateComparatorAccessor(firstChar);
		Comparator<String> cmpFirstViaTradicional = Comparator.comparing(firstChar);
		
		Comparator<String> cmpSecondViaBuilder = ComparatorBuilder.generateComparatorAccessor(secondChar);
		Comparator<String> cmpSecondViaTradicional = Comparator.comparing(secondChar);
		
		Comparator<String> cmpThirdViaBuilder = ComparatorBuilder.generateComparatorAccessor(thirdChar);
		Comparator<String> cmpThirdViaTradicional = Comparator.comparing(thirdChar);
		
		doQuadTest(cmpFirstViaBuilder, cmpFirstViaTradicional);
		doQuadTest(cmpSecondViaBuilder, cmpSecondViaTradicional);
		doQuadTest(cmpThirdViaBuilder, cmpThirdViaTradicional);
		
		Comparator<String> f_s_viaBuilder = ComparatorBuilder.start(firstChar).adicionaDesempate(secondChar).build();
		Comparator<String> f_s_viaTradicional = Comparator.comparing(firstChar).thenComparing(secondChar);
		doQuadTest(f_s_viaBuilder, f_s_viaTradicional);
		
		Comparator<String> s_f_viaBuilder = ComparatorBuilder.start(secondChar).adicionaDesempate(firstChar).build();
		Comparator<String> s_f_viaTradicional = Comparator.comparing(secondChar).thenComparing(firstChar);
		doQuadTest(s_f_viaBuilder, s_f_viaTradicional);
		

		Comparator<String> f_t_viaBuilder = ComparatorBuilder.start(firstChar).adicionaDesempate(thirdChar).build();
		Comparator<String> f_t_viaTradicional = Comparator.comparing(firstChar).thenComparing(thirdChar);
		doQuadTest(f_t_viaBuilder, f_t_viaTradicional);
		
		Comparator<String> t_f_viaBuilder = ComparatorBuilder.start(thirdChar).adicionaDesempate(firstChar).build();
		Comparator<String> t_f_viaTradicional = Comparator.comparing(thirdChar).thenComparing(firstChar);
		doQuadTest(t_f_viaBuilder, t_f_viaTradicional);
		
		Comparator<String> f_s_t_viaBuilder = ComparatorBuilder.start(firstChar).adicionaDesempate(secondChar).adicionaDesempate(thirdChar).build();
		Comparator<String> f_s_t_viaTradicional = Comparator.comparing(firstChar).thenComparing(secondChar).thenComparing(thirdChar);
		doQuadTest(f_s_t_viaBuilder, f_s_t_viaTradicional);
	}

	@Test
	public void testeComparatorBuilderSemComparable() {
		// O teste aqui vai ser do tamanho da lista
		// como desempate vai ser
		Indirection<List<Object>> o1, o2;
		o1 = new Indirection<>(new ArrayList<>());
		o2 = new Indirection<>(new ArrayList<>());

		o1.x.add("0");
		o2.x.add("0");
		o2.x.add("1");

		Comparator<Indirection<List<Object>>> cmp = ComparatorBuilder.generateComparatorAccessor(Indirection::getX, (l1, l2) -> l1.size() - l2.size());
		assertThat(cmp.compare(o1, o1), equalTo(0));
		assertThat(cmp.compare(o2, o2), equalTo(0));
		assertThat(cmp.compare(o1, o2), lessThan(0));
		assertThat(cmp.compare(o2, o1), greaterThan(0));
	}

	@Test
	public void testeComparatorNullSafe() {
		// O teste aqui vai ser se a indireção com o nulo
		Indirection<Integer> o1, o2, oNull;
		o1 = new Indirection<>(1);
		o2 = new Indirection<>(2);
		oNull = new Indirection<>(null);

		Comparator<Indirection<Integer>> cmpNullFirst = ComparatorBuilder.generateComparatorAccessor(Indirection::getX, ComparatorUtils.nullFirst());
		Comparator<Indirection<Integer>> cmpNullLast = ComparatorBuilder.generateComparatorAccessor(Indirection::getX, ComparatorUtils.nullLast());

		assertThat(cmpNullFirst.compare(o1, o1), equalTo(0));
		assertThat(cmpNullFirst.compare(o2, o2), equalTo(0));
		assertThat(cmpNullFirst.compare(o1, o2), lessThan(0));
		assertThat(cmpNullFirst.compare(o2, o1), greaterThan(0));

		assertThat(cmpNullFirst.compare(oNull, oNull), equalTo(0));
		assertThat(cmpNullFirst.compare(o1, oNull), greaterThan(0));
		assertThat(cmpNullFirst.compare(o2, oNull), greaterThan(0));
		assertThat(cmpNullFirst.compare(oNull, o1), lessThan(0));
		assertThat(cmpNullFirst.compare(oNull, o2), lessThan(0));

		assertThat(cmpNullLast.compare(o1, o1), equalTo(0));
		assertThat(cmpNullLast.compare(o2, o2), equalTo(0));
		assertThat(cmpNullLast.compare(o1, o2), lessThan(0));
		assertThat(cmpNullLast.compare(o2, o1), greaterThan(0));

		assertThat(cmpNullLast.compare(oNull, oNull), equalTo(0));
		assertThat(cmpNullLast.compare(o1, oNull), lessThan(0));
		assertThat(cmpNullLast.compare(o2, oNull), lessThan(0));
		assertThat(cmpNullLast.compare(oNull, o1), greaterThan(0));
		assertThat(cmpNullLast.compare(oNull, o2), greaterThan(0));

		Stream<Indirection<Integer>> streamBase = Stream.of(o1, o2, oNull);

		List<Indirection<Integer>> lSortedNullFirst = streamBase.sorted(cmpNullFirst).collect(Collectors.toList());

		assertThat(lSortedNullFirst.get(0), equalTo(oNull));
		assertThat(lSortedNullFirst.get(1), equalTo(o1));
		assertThat(lSortedNullFirst.get(2), equalTo(o2));

		List<Indirection<Integer>> lSortedNullLast = streamBase.sorted(cmpNullLast).collect(Collectors.toList());

		assertThat(lSortedNullLast.get(0), equalTo(o1));
		assertThat(lSortedNullLast.get(1), equalTo(o2));
		assertThat(lSortedNullLast.get(2), equalTo(oNull));
	}

	@Test
	public void testeInversao() {
		Indirection<Integer> o1, o2, o3, oNull;
		o1 = new Indirection<>(1);
		o2 = new Indirection<>(2);
		o3 = new Indirection<>(3);
		oNull = new Indirection<>(null);
		Stream<Indirection<Integer>> streamBase = Stream.of(
			o2,
			o3,
			o1,
			oNull
		);
		Comparator<Indirection<Integer>> cmpNullFirst = ComparatorBuilder.generateComparatorAccessor(Indirection::getX, ComparatorUtils.nullFirst());

		List<Indirection<Integer>> lSortedInvertido = streamBase.sorted(ComparatorUtils.reversed(cmpNullFirst)).collect(Collectors.toList());
		assertThat(lSortedInvertido.get(0), equalTo(o3));
		assertThat(lSortedInvertido.get(1), equalTo(o2));
		assertThat(lSortedInvertido.get(2), equalTo(o1));
		assertThat(lSortedInvertido.get(3), equalTo(oNull));
	}

	private void doQuadTest(Comparator<String> cmpViaBuilder, Comparator<String> cmpViaTradicional) {
		for (String testCaseA: testes) {
			for (String testCaseB: testes) {
				assertThat(cmpViaBuilder.compare(testCaseA, testCaseB), equalTo(cmpViaTradicional.compare(testCaseA, testCaseB)));
			}
		}
		
	}
}
