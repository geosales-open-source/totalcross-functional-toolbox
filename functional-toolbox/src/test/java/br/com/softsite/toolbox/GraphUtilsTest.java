package br.com.softsite.toolbox;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.junit.Test;

import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.graph.GraphUtils;

public class GraphUtilsTest {

	@Test
	public void grafoAciclico() {
		Function<Integer, List<Integer>> pegaVizinhos = partida -> {
			switch (partida) {
			case 0:
				return CollectionUtils.toList(1, 2);
			case 1:
				return CollectionUtils.toSingletonList(2);
			case 2:
				return CollectionUtils.toSingletonList(3);
			}
			return new ArrayList<>();
		};
		assertTrue("grafo passado é acíclico", GraphUtils.grafoAciclico(0, pegaVizinhos));
	}

	@Test
	public void grafoCiclicoInalcancavel() {
		Function<Integer, List<Integer>> pegaVizinhos = partida -> {
			switch (partida) {
			case 0:
				return CollectionUtils.toList(1, 2);
			case 1:
				return CollectionUtils.toSingletonList(2);
			case 2:
				return CollectionUtils.toSingletonList(3);
			case 4:
				return CollectionUtils.toSingletonList(4);
			}
			return new ArrayList<>();
		};
		assertTrue("grafo passado é cíclico, porém não há ciclos nesse ponto de entrada", GraphUtils.grafoAciclico(0, pegaVizinhos));
	}

	@Test
	public void grafoCiclicoTrivial() {
		Function<Integer, List<Integer>> pegaVizinhos = partida -> {
			switch (partida) {
			case 0:
				return CollectionUtils.toList(1, 2);
			case 1:
				return CollectionUtils.toSingletonList(2);
			case 2:
				return CollectionUtils.toSingletonList(3);
			case 4:
				return CollectionUtils.toSingletonList(4);
			}
			return new ArrayList<>();
		};
		assertFalse("grafo passado é cíclico", GraphUtils.grafoAciclico(CollectionUtils.toList(0, 1, 2, 3, 4), pegaVizinhos));
	}

	@Test
	public void grafoCiclico() {
		Function<Integer, List<Integer>> pegaVizinhos = partida -> {
			switch (partida) {
			case 0:
				return CollectionUtils.toList(1, 2);
			case 1:
				return CollectionUtils.toSingletonList(2);
			case 2:
				return CollectionUtils.toList(3, 4);
			case 4:
				return CollectionUtils.toList(0, 2);
			}
			return new ArrayList<>();
		};
		assertFalse("grafo passado é cíclico", GraphUtils.grafoAciclico(CollectionUtils.toList(0, 1, 2, 3, 4), pegaVizinhos));
	}
}
