package br.com.softsite.toolbox;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.junit.Test;

import br.com.softsite.toolbox.indirection.BooleanIndirection;
import br.com.softsite.toolbox.indirection.IntIndirection;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.optional.Optional;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.DoubleStream;
import br.com.softsite.toolbox.stream.IntStream;
import br.com.softsite.toolbox.stream.LongStream;
import br.com.softsite.toolbox.stream.Stream;
import br.com.softsite.toolbox.stream.StreamHolder;

public class StreamTest {

	@Test
	public void test() {
		Stream<Integer> stream = Stream.<Integer>builder().add(0).add(13).add(0).add(1).add(null).add(2).add(null).add(7).add(65).build();

		Optional<Integer> x;

		x = stream.filter(i -> i != null).max(Integer::compare);

		assertThat("Não pode ser nulo", x.isPresent());
		assertThat(x.get(), equalTo(65));

		x = stream.filter(i -> i != null).filter(i -> i % 2 == 0).max(Integer::compare);
		assertThat("Não pode ser nulo", x.isPresent());
		assertThat(x.get(), equalTo(2));

		stream.filter(i -> i != null).peek(i -> System.out.println("i " + i + "; classe " + i.getClass())).map(String::valueOf).forEach(i -> System.out.println("i " + i + "; classe " + i.getClass()));
		double d = stream.filter(i -> i != null).collect(Collectors.summingDouble(Integer::doubleValue));
		System.out.println("valor obtido " + d);

		stream.filter(i -> i != null).flatMap((i) -> {
			if (i.equals(0)) {
				return Stream.empty();
			} else if (i.equals(1)) {
				return Stream.of(i);
			}
			Stream.Builder<Integer> builder = Stream.<Integer>builder().add(1);

			for (int dc = 2; dc < i; dc++) {
				if (i % dc == 0) {
					builder.accept(dc);
				}
			}

			return builder.add(i).build();
		}).forEach(i -> System.out.println("imprimindo os divisores " + i));

		stream.filter(i -> i != null).flatMap((i) -> {
			if (i.equals(0)) {
				return Stream.empty();
			} else if (i.equals(1)) {
				return Stream.of(Pair.getPair(i, i));
			}
			Stream.Builder<Pair<Integer, Integer>> builder = Stream.<Pair<Integer, Integer>>builder().add(Pair.getPair(i, 1));

			for (int dc = 2; dc < i; dc++) {
				if (i % dc == 0) {
					builder.accept(Pair.getPair(i, dc));
				}
			}

			return builder.add(Pair.getPair(i, i)).build();
		}).forEach(i -> System.out.println("imprimindo os divisores " + i));

		new Stream<>(stream.filter(i -> i != null).flatMap((i) -> {
			if (i.equals(0)) {
				return Stream.empty();
			} else if (i.equals(1)) {
				return Stream.of(Pair.getPair(1, i));
			}
			Stream.Builder<Pair<Integer, Integer>> builder = Stream.<Pair<Integer, Integer>>builder().add(Pair.getPair(i, 1));

			for (int dc = 2; dc < i; dc++) {
				if (i % dc == 0) {
					builder.accept(Pair.getPair(i, dc));
				}
			}

			return builder.add(Pair.getPair(i, i)).build();
		}).collect(Collectors.<Pair<Integer,Integer>, Integer, List<Integer>>toMap(Pair::getKey, p -> {
			ArrayList<Integer> a = new ArrayList<>();
			a.add(p.getValue());
			return a;
		}, (l1, l2) -> {
			l1.addAll(l2);
			return l1;
		})).entrySet()).filter(e -> e.getValue().size() == 2).forEach(p -> System.out.println("o número " + p.getKey() + " é primo"));

		stream.takeWhile(i -> i != null).forEach(i -> System.out.println("valor do i " + i));
	}

	@Test
	public void testSum() {
		IntStream is = IntStream.range(1, 5);
		assertThat("soma deve ser 10", is.sum(), equalTo(10));

		LongStream ls = LongStream.range(1, 5);
		assertThat("soma deve ser 10L", ls.sum(), equalTo(10L));

		DoubleStream ds = DoubleStream.of(1, 2, 3, 4);
		assertThat("soma deve ser 10.0", ds.sum(), equalTo(10.0));
	}

	@Test
	public void testLimit() {
		Stream<String> s = Stream.generate(() -> "?").limit(5);
		List<String> a = s.collect(Collectors.toList());

		assertThat("tamanho da lista deve ser 5", a.size(), equalTo(5));
	}

	@Test
	public void testJoining() {
		Stream<String> s = Stream.generate(() -> "?").limit(3);
		String qry = s.collect(Collectors.joining(",", "(", ")"));

		assertThat("qry deve ser (?,?,?)", qry, equalTo("(?,?,?)"));
	}

	@Test
	public void callingClose() {
		BooleanIndirection ind1 = new BooleanIndirection(false);
		BooleanIndirection ind2 = new BooleanIndirection(false);
		Stream<String> s = Stream.generate(() -> "?").onClose(() -> ind1.x = true).limit(3).onClose(() -> ind2.x = true);
		try (StreamHolder<Stream<String>> h = new StreamHolder<>(s)) {
			assertThat("apenas 3 elementos na listagem", s.count(), equalTo(3L));
		}
		assertTrue("primeiro close deveria ser chamado", ind1.x);
		assertTrue("segundo close deveria ser chamado", ind2.x);
	}

	@Test
	public void callingCloseOrdered() {
		IntIndirection ind1 = new IntIndirection(0);
		IntIndirection ind2 = new IntIndirection(0);
		Stream<String> s = Stream.generate(() -> "?").onClose(() -> ind1.x = ind2.x + 1).limit(3).onClose(() -> ind2.x = 1);
		try (StreamHolder<Stream<String>> h = new StreamHolder<>(s)) {
			assertThat("apenas 3 elementos na listagem", s.count(), equalTo(3L));
		}
		assertEquals("segundo close deveria ser chamado", 1, ind2.x);
		assertEquals("primeiro close deveria ser chamado depois do segundo", 2, ind1.x);
	}

	private class PrivException extends RuntimeException {
		private static final long serialVersionUID = -1062779857288867034L;
	}

	@Test
	public void callingCloseExploding() {
		PrivException expected = null;
		BooleanIndirection ind1 = new BooleanIndirection(false);
		BooleanIndirection ind2 = new BooleanIndirection(false);
		Function<BooleanIndirection, Runnable> createCloseExploding = i -> () -> {
			i.x = true;
			throw new PrivException();
		};
		Stream<String> s = Stream.generate(() -> "?").onClose(createCloseExploding.apply(ind1)).limit(3).onClose(createCloseExploding.apply(ind2));
		try (StreamHolder<Stream<String>> h = new StreamHolder<>(s)) {
			assertThat("apenas 3 elementos na listagem", s.count(), equalTo(3L));
		} catch (PrivException e) {
			expected = e;
		}
		assertNotNull("exceção deveria ter sido disparada", expected);
		assertEquals("exceção capturada deve ter uma exceção suprimida", 1, expected.getSuppressed().length);
		assertTrue("exceção suprimida deve ser da clase PrivException", PrivException.class.isInstance(expected.getSuppressed()[0]));

		assertTrue("primeiro close deveria ser chamado", ind1.x);
		assertTrue("segundo close deveria ser chamado", ind2.x);
	}

	@Test
	public void noneMatch() {
		assertFalse("Alguns devem não ser pares", IntStream.range(0, 10).noneMatch(StreamTest::par));
		assertFalse("Alguns devem não ser pares", IntStream.range(0, 10).boxed().noneMatch(StreamTest::par));
		assertFalse("Alguns devem não ser pares", LongStream.range(0, 10).noneMatch(StreamTest::par));
		assertFalse("Alguns devem não ser pares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).noneMatch(StreamTest::par));

		assertFalse("Todos são pares", IntStream.range(0, 10).map(x -> 2*x).noneMatch(StreamTest::par));
		assertFalse("Todos são pares", IntStream.range(0, 10).map(x -> 2*x).boxed().noneMatch(StreamTest::par));
		assertFalse("Todos são pares", LongStream.range(0, 10).map(x -> 2*x).noneMatch(StreamTest::par));
		assertFalse("Todos são pares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> 2*x).noneMatch(StreamTest::par));

		assertTrue("Todos são ímpares", IntStream.range(0, 10).map(x -> 2*x + 1).noneMatch(StreamTest::par));
		assertTrue("Todos são ímpares", IntStream.range(0, 10).map(x -> 2*x + 1).boxed().noneMatch(StreamTest::par));
		assertTrue("Todos são ímpares", LongStream.range(0, 10).map(x -> 2*x + 1).noneMatch(StreamTest::par));
		assertTrue("Todos são ímpares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> 2*x + 1).noneMatch(StreamTest::par));
	}

	@Test
	public void anyMatch() {
		assertTrue("Alguns devem não ser pares", IntStream.range(0, 10).anyMatch(StreamTest::par));
		assertTrue("Alguns devem não ser pares", IntStream.range(0, 10).boxed().anyMatch(StreamTest::par));
		assertTrue("Alguns devem não ser pares", LongStream.range(0, 10).anyMatch(StreamTest::par));
		assertTrue("Alguns devem não ser pares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).anyMatch(StreamTest::par));

		assertTrue("Todos são pares", IntStream.range(0, 10).map(x -> 2*x).anyMatch(StreamTest::par));
		assertTrue("Todos são pares", IntStream.range(0, 10).map(x -> 2*x).boxed().anyMatch(StreamTest::par));
		assertTrue("Todos são pares", LongStream.range(0, 10).map(x -> 2*x).anyMatch(StreamTest::par));
		assertTrue("Todos são pares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> 2*x).anyMatch(StreamTest::par));

		assertFalse("Todos são ímpares", IntStream.range(0, 10).map(x -> 2*x + 1).anyMatch(StreamTest::par));
		assertFalse("Todos são ímpares", IntStream.range(0, 10).map(x -> 2*x + 1).boxed().anyMatch(StreamTest::par));
		assertFalse("Todos são ímpares", LongStream.range(0, 10).map(x -> 2*x + 1).anyMatch(StreamTest::par));
		assertFalse("Todos são ímpares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> 2*x + 1).anyMatch(StreamTest::par));
	}

	@Test
	public void allMatch() {
		assertFalse("Alguns devem não ser pares", IntStream.range(0, 10).allMatch(StreamTest::par));
		assertFalse("Alguns devem não ser pares", IntStream.range(0, 10).boxed().allMatch(StreamTest::par));
		assertFalse("Alguns devem não ser pares", LongStream.range(0, 10).allMatch(StreamTest::par));
		assertFalse("Alguns devem não ser pares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).allMatch(StreamTest::par));

		assertTrue("Todos são pares", IntStream.range(0, 10).map(x -> 2*x).allMatch(StreamTest::par));
		assertTrue("Todos são pares", IntStream.range(0, 10).map(x -> 2*x).boxed().allMatch(StreamTest::par));
		assertTrue("Todos são pares", LongStream.range(0, 10).map(x -> 2*x).allMatch(StreamTest::par));
		assertTrue("Todos são pares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> 2*x).allMatch(StreamTest::par));

		assertFalse("Todos são ímpares", IntStream.range(0, 10).map(x -> 2*x + 1).allMatch(StreamTest::par));
		assertFalse("Todos são ímpares", IntStream.range(0, 10).map(x -> 2*x + 1).boxed().allMatch(StreamTest::par));
		assertFalse("Todos são ímpares", LongStream.range(0, 10).map(x -> 2*x + 1).allMatch(StreamTest::par));
		assertFalse("Todos são ímpares", DoubleStream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).map(x -> 2*x + 1).allMatch(StreamTest::par));
	}

	@Test
	public void testDistinct() {
		assertEquals("só tem 1 elemento único", 1, Stream.generate(() -> "?").limit(3).distinct().count());
		assertEquals("só tem 2 elementos distintos mod 2", 2, IntStream.range(0, 10).map(x ->  x % 2).boxed().distinct().count());
		assertEquals("só tem 10 elementos distintos mod 10", 10, IntStream.range(0, 100000).map(x ->  x % 10).boxed().distinct().count());

		assertEquals("só tem 2 elementos distintos mod 2", 2, IntStream.range(0, 10).map(x ->  x % 2).distinct().count());
		assertEquals("só tem 10 elementos distintos mod 10", 10, IntStream.range(0, 100000).map(x ->  x % 10).distinct().count());
	}

	@Test
	public void testCollector() {
		List<Pair<String, String>> l = Arrays.asList(Pair.getPair("Data", "abc"), Pair.getPair("other", "m"), Pair.getPair("data", "def"), Pair.getPair("marm", ""), Pair.getPair("marm", "ota"), Pair.getPair("marmota", ""));
		Map<String, String> m = new Stream<>(l)
				.collect(
						Collectors.groupingBy(
								p -> p.getKey().toLowerCase(), // agrupando pela chave, case insensitive
								Collectors.mapping( // agrupo os valores mapeando para
										Pair::getValue, // o valor
										Collectors.mapping( // ignorando espaços no fim
												String::trim,
												Collectors.filtering( // removendo antes de juntar
														FunctionUtils.negate(String::isEmpty), // as strings vazias
														Collectors.mapping( // apenas por teimosia porque CharSequence não contém isEmpty, e o argumento do joining precisa ser charsequence, e não string
																FunctionUtils::identity,
																Collectors.joining(", ") // daí juntando o que restou separado por vírgulas
														)
												)
										)
								)
						)
				);

		assertThat(m.size(), equalTo(4));
		assertThat(m.get("other"), equalTo("m"));
		assertThat(m.get("data"), equalTo("abc, def"));
		assertThat(m.get("marm"), equalTo("ota"));
		assertThat(m.get("marmota"), equalTo(""));
	}

	private static boolean par(int n) {
		return n % 2 == 0;
	}

	private static boolean par(long n) {
		return n % 2 == 0;
	}

	private static boolean par(double n) {
		int x = (int) n;
		if (x - n != 0) {
			return false;
		}
		return x % 2 == 0;
	}
}
