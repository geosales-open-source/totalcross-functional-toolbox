#!/bin/bash

rm -r smoke
if [[ "$VERSION" == *-retrolambda ]]; then
	extra_profile="-P retrolambda"
	# aplica a resolução da variável 'revision' para '-retrolambda' no bom
	sed -i -E -e 's/\$\{revision\}/-retrolambda/g' bom/pom.xml
fi
./mvnw -pl bom,functional-toolbox -P jitpack $extra_profile install -DskipTests
